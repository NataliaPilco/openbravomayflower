//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.FinancialAccount;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class Depositos1CA823322A874B879DEAA2577476814AData implements FieldProvider {
static Logger log4j = Logger.getLogger(Depositos1CA823322A874B879DEAA2577476814AData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String line;
  public String statementdate;
  public String emDpDeposito;
  public String finPaymentId;
  public String finPaymentIdr;
  public String emAprmModify;
  public String finFinaccTransactionId;
  public String mProductId;
  public String cBpartnerId;
  public String isactive;
  public String status;
  public String statusr;
  public String finFinancialAccountId;
  public String depositamt;
  public String paymentamt;
  public String processed;
  public String processing;
  public String cleared;
  public String reconciled;
  public String user1Id;
  public String user2Id;
  public String cCurrencyId;
  public String cCurrencyIdr;
  public String cActivityId;
  public String trxtype;
  public String trxtyper;
  public String description;
  public String cCampaignId;
  public String cGlitemId;
  public String cGlitemIdr;
  public String cProjectId;
  public String dateacct;
  public String foreignAmount;
  public String finReconciliationId;
  public String finReconciliationIdr;
  public String foreignConvertRate;
  public String createdbyalgorithm;
  public String deleteBtn;
  public String foreignCurrencyId;
  public String cSalesregionId;
  public String paymentdocno;
  public String forcedTableId;
  public String adClientId;
  public String dpFinaccTransactionVId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("line"))
      return line;
    else if (fieldName.equalsIgnoreCase("statementdate"))
      return statementdate;
    else if (fieldName.equalsIgnoreCase("em_dp_deposito") || fieldName.equals("emDpDeposito"))
      return emDpDeposito;
    else if (fieldName.equalsIgnoreCase("fin_payment_id") || fieldName.equals("finPaymentId"))
      return finPaymentId;
    else if (fieldName.equalsIgnoreCase("fin_payment_idr") || fieldName.equals("finPaymentIdr"))
      return finPaymentIdr;
    else if (fieldName.equalsIgnoreCase("em_aprm_modify") || fieldName.equals("emAprmModify"))
      return emAprmModify;
    else if (fieldName.equalsIgnoreCase("fin_finacc_transaction_id") || fieldName.equals("finFinaccTransactionId"))
      return finFinaccTransactionId;
    else if (fieldName.equalsIgnoreCase("m_product_id") || fieldName.equals("mProductId"))
      return mProductId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("status"))
      return status;
    else if (fieldName.equalsIgnoreCase("statusr"))
      return statusr;
    else if (fieldName.equalsIgnoreCase("fin_financial_account_id") || fieldName.equals("finFinancialAccountId"))
      return finFinancialAccountId;
    else if (fieldName.equalsIgnoreCase("depositamt"))
      return depositamt;
    else if (fieldName.equalsIgnoreCase("paymentamt"))
      return paymentamt;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("processing"))
      return processing;
    else if (fieldName.equalsIgnoreCase("cleared"))
      return cleared;
    else if (fieldName.equalsIgnoreCase("reconciled"))
      return reconciled;
    else if (fieldName.equalsIgnoreCase("user1_id") || fieldName.equals("user1Id"))
      return user1Id;
    else if (fieldName.equalsIgnoreCase("user2_id") || fieldName.equals("user2Id"))
      return user2Id;
    else if (fieldName.equalsIgnoreCase("c_currency_id") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("c_currency_idr") || fieldName.equals("cCurrencyIdr"))
      return cCurrencyIdr;
    else if (fieldName.equalsIgnoreCase("c_activity_id") || fieldName.equals("cActivityId"))
      return cActivityId;
    else if (fieldName.equalsIgnoreCase("trxtype"))
      return trxtype;
    else if (fieldName.equalsIgnoreCase("trxtyper"))
      return trxtyper;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("c_campaign_id") || fieldName.equals("cCampaignId"))
      return cCampaignId;
    else if (fieldName.equalsIgnoreCase("c_glitem_id") || fieldName.equals("cGlitemId"))
      return cGlitemId;
    else if (fieldName.equalsIgnoreCase("c_glitem_idr") || fieldName.equals("cGlitemIdr"))
      return cGlitemIdr;
    else if (fieldName.equalsIgnoreCase("c_project_id") || fieldName.equals("cProjectId"))
      return cProjectId;
    else if (fieldName.equalsIgnoreCase("dateacct"))
      return dateacct;
    else if (fieldName.equalsIgnoreCase("foreign_amount") || fieldName.equals("foreignAmount"))
      return foreignAmount;
    else if (fieldName.equalsIgnoreCase("fin_reconciliation_id") || fieldName.equals("finReconciliationId"))
      return finReconciliationId;
    else if (fieldName.equalsIgnoreCase("fin_reconciliation_idr") || fieldName.equals("finReconciliationIdr"))
      return finReconciliationIdr;
    else if (fieldName.equalsIgnoreCase("foreign_convert_rate") || fieldName.equals("foreignConvertRate"))
      return foreignConvertRate;
    else if (fieldName.equalsIgnoreCase("createdbyalgorithm"))
      return createdbyalgorithm;
    else if (fieldName.equalsIgnoreCase("delete_btn") || fieldName.equals("deleteBtn"))
      return deleteBtn;
    else if (fieldName.equalsIgnoreCase("foreign_currency_id") || fieldName.equals("foreignCurrencyId"))
      return foreignCurrencyId;
    else if (fieldName.equalsIgnoreCase("c_salesregion_id") || fieldName.equals("cSalesregionId"))
      return cSalesregionId;
    else if (fieldName.equalsIgnoreCase("paymentdocno"))
      return paymentdocno;
    else if (fieldName.equalsIgnoreCase("forced_table_id") || fieldName.equals("forcedTableId"))
      return forcedTableId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("dp_finacc_transaction_v_id") || fieldName.equals("dpFinaccTransactionVId"))
      return dpFinaccTransactionVId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Depositos1CA823322A874B879DEAA2577476814AData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String finFinancialAccountId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, finFinancialAccountId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Depositos1CA823322A874B879DEAA2577476814AData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String finFinancialAccountId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(dp_finacc_transaction_v.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = dp_finacc_transaction_v.CreatedBy) as CreatedByR, " +
      "        to_char(dp_finacc_transaction_v.Updated, ?) as updated, " +
      "        to_char(dp_finacc_transaction_v.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        dp_finacc_transaction_v.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = dp_finacc_transaction_v.UpdatedBy) as UpdatedByR," +
      "        dp_finacc_transaction_v.AD_Org_ID, " +
      "dp_finacc_transaction_v.Line, " +
      "dp_finacc_transaction_v.Statementdate, " +
      "dp_finacc_transaction_v.EM_Dp_Deposito, " +
      "dp_finacc_transaction_v.FIN_Payment_ID, " +
      "(CASE WHEN dp_finacc_transaction_v.FIN_Payment_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.DocumentNo), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(table1.Paymentdate, 'DD-MM-YYYY')),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Amount), ''))),'') ) END) AS FIN_Payment_IDR, " +
      "dp_finacc_transaction_v.EM_Aprm_Modify, " +
      "dp_finacc_transaction_v.FIN_Finacc_Transaction_ID, " +
      "dp_finacc_transaction_v.M_Product_ID, " +
      "dp_finacc_transaction_v.C_Bpartner_ID, " +
      "COALESCE(dp_finacc_transaction_v.Isactive, 'N') AS Isactive, " +
      "dp_finacc_transaction_v.Status, " +
      "(CASE WHEN dp_finacc_transaction_v.Status IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS StatusR, " +
      "dp_finacc_transaction_v.FIN_Financial_Account_ID, " +
      "dp_finacc_transaction_v.Depositamt, " +
      "dp_finacc_transaction_v.Paymentamt, " +
      "COALESCE(dp_finacc_transaction_v.Processed, 'N') AS Processed, " +
      "COALESCE(dp_finacc_transaction_v.Processing, 'N') AS Processing, " +
      "COALESCE(dp_finacc_transaction_v.Cleared, 'N') AS Cleared, " +
      "COALESCE(dp_finacc_transaction_v.Reconciled, 'N') AS Reconciled, " +
      "dp_finacc_transaction_v.User1_ID, " +
      "dp_finacc_transaction_v.User2_ID, " +
      "dp_finacc_transaction_v.C_Currency_ID, " +
      "(CASE WHEN dp_finacc_transaction_v.C_Currency_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.ISO_Code), ''))),'') ) END) AS C_Currency_IDR, " +
      "dp_finacc_transaction_v.C_Activity_ID, " +
      "dp_finacc_transaction_v.Trxtype, " +
      "(CASE WHEN dp_finacc_transaction_v.Trxtype IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS TrxtypeR, " +
      "dp_finacc_transaction_v.Description, " +
      "dp_finacc_transaction_v.C_Campaign_ID, " +
      "dp_finacc_transaction_v.C_Glitem_ID, " +
      "(CASE WHEN dp_finacc_transaction_v.C_Glitem_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS C_Glitem_IDR, " +
      "dp_finacc_transaction_v.C_Project_ID, " +
      "dp_finacc_transaction_v.Dateacct, " +
      "dp_finacc_transaction_v.Foreign_Amount, " +
      "dp_finacc_transaction_v.FIN_Reconciliation_ID, " +
      "(CASE WHEN dp_finacc_transaction_v.FIN_Reconciliation_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.DocumentNo), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(table5.Statementdate, 'DD-MM-YYYY')),'') ) END) AS FIN_Reconciliation_IDR, " +
      "dp_finacc_transaction_v.Foreign_Convert_Rate, " +
      "COALESCE(dp_finacc_transaction_v.Createdbyalgorithm, 'N') AS Createdbyalgorithm, " +
      "dp_finacc_transaction_v.Delete_Btn, " +
      "dp_finacc_transaction_v.Foreign_Currency_ID, " +
      "dp_finacc_transaction_v.C_Salesregion_ID, " +
      "dp_finacc_transaction_v.Paymentdocno, " +
      "dp_finacc_transaction_v.Forced_Table_ID, " +
      "dp_finacc_transaction_v.AD_Client_ID, " +
      "dp_finacc_transaction_v.dp_finacc_transaction_v_id, " +
      "        ? AS LANGUAGE " +
      "        FROM dp_finacc_transaction_v left join (select FIN_Payment_ID, DocumentNo, Paymentdate, C_Bpartner_ID, Amount from FIN_Payment) table1 on (dp_finacc_transaction_v.FIN_Payment_ID = table1.FIN_Payment_ID) left join (select C_BPartner_ID, Name from C_BPartner) table2 on (table1.C_Bpartner_ID = table2.C_BPartner_ID) left join ad_ref_list_v list1 on (dp_finacc_transaction_v.Status = list1.value and list1.ad_reference_id = '575BCB88A4694C27BC013DE9C73E6FE7' and list1.ad_language = ?)  left join (select C_Currency_ID, ISO_Code from C_Currency) table3 on (dp_finacc_transaction_v.C_Currency_ID = table3.C_Currency_ID) left join ad_ref_list_v list2 on (dp_finacc_transaction_v.Trxtype = list2.value and list2.ad_reference_id = '4EFC9773F30B4ACE97D225BD13CFF8CB' and list2.ad_language = ?)  left join (select C_Glitem_ID, Name from C_Glitem) table4 on (dp_finacc_transaction_v.C_Glitem_ID = table4.C_Glitem_ID) left join (select FIN_Reconciliation_ID, DocumentNo, Statementdate from FIN_Reconciliation) table5 on (dp_finacc_transaction_v.FIN_Reconciliation_ID = table5.FIN_Reconciliation_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((finFinancialAccountId==null || finFinancialAccountId.equals(""))?"":"  AND dp_finacc_transaction_v.FIN_Financial_Account_ID = ?  ");
    strSql = strSql + 
      "        AND dp_finacc_transaction_v.dp_finacc_transaction_v_id = ? " +
      "        AND dp_finacc_transaction_v.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND dp_finacc_transaction_v.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (finFinancialAccountId != null && !(finFinancialAccountId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Depositos1CA823322A874B879DEAA2577476814AData objectDepositos1CA823322A874B879DEAA2577476814AData = new Depositos1CA823322A874B879DEAA2577476814AData();
        objectDepositos1CA823322A874B879DEAA2577476814AData.created = UtilSql.getValue(result, "created");
        objectDepositos1CA823322A874B879DEAA2577476814AData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectDepositos1CA823322A874B879DEAA2577476814AData.updated = UtilSql.getValue(result, "updated");
        objectDepositos1CA823322A874B879DEAA2577476814AData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectDepositos1CA823322A874B879DEAA2577476814AData.updatedby = UtilSql.getValue(result, "updatedby");
        objectDepositos1CA823322A874B879DEAA2577476814AData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectDepositos1CA823322A874B879DEAA2577476814AData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.line = UtilSql.getValue(result, "line");
        objectDepositos1CA823322A874B879DEAA2577476814AData.statementdate = UtilSql.getDateValue(result, "statementdate", "dd-MM-yyyy");
        objectDepositos1CA823322A874B879DEAA2577476814AData.emDpDeposito = UtilSql.getValue(result, "em_dp_deposito");
        objectDepositos1CA823322A874B879DEAA2577476814AData.finPaymentId = UtilSql.getValue(result, "fin_payment_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.finPaymentIdr = UtilSql.getValue(result, "fin_payment_idr");
        objectDepositos1CA823322A874B879DEAA2577476814AData.emAprmModify = UtilSql.getValue(result, "em_aprm_modify");
        objectDepositos1CA823322A874B879DEAA2577476814AData.finFinaccTransactionId = UtilSql.getValue(result, "fin_finacc_transaction_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.mProductId = UtilSql.getValue(result, "m_product_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.isactive = UtilSql.getValue(result, "isactive");
        objectDepositos1CA823322A874B879DEAA2577476814AData.status = UtilSql.getValue(result, "status");
        objectDepositos1CA823322A874B879DEAA2577476814AData.statusr = UtilSql.getValue(result, "statusr");
        objectDepositos1CA823322A874B879DEAA2577476814AData.finFinancialAccountId = UtilSql.getValue(result, "fin_financial_account_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.depositamt = UtilSql.getValue(result, "depositamt");
        objectDepositos1CA823322A874B879DEAA2577476814AData.paymentamt = UtilSql.getValue(result, "paymentamt");
        objectDepositos1CA823322A874B879DEAA2577476814AData.processed = UtilSql.getValue(result, "processed");
        objectDepositos1CA823322A874B879DEAA2577476814AData.processing = UtilSql.getValue(result, "processing");
        objectDepositos1CA823322A874B879DEAA2577476814AData.cleared = UtilSql.getValue(result, "cleared");
        objectDepositos1CA823322A874B879DEAA2577476814AData.reconciled = UtilSql.getValue(result, "reconciled");
        objectDepositos1CA823322A874B879DEAA2577476814AData.user1Id = UtilSql.getValue(result, "user1_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.user2Id = UtilSql.getValue(result, "user2_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.cCurrencyId = UtilSql.getValue(result, "c_currency_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.cCurrencyIdr = UtilSql.getValue(result, "c_currency_idr");
        objectDepositos1CA823322A874B879DEAA2577476814AData.cActivityId = UtilSql.getValue(result, "c_activity_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.trxtype = UtilSql.getValue(result, "trxtype");
        objectDepositos1CA823322A874B879DEAA2577476814AData.trxtyper = UtilSql.getValue(result, "trxtyper");
        objectDepositos1CA823322A874B879DEAA2577476814AData.description = UtilSql.getValue(result, "description");
        objectDepositos1CA823322A874B879DEAA2577476814AData.cCampaignId = UtilSql.getValue(result, "c_campaign_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.cGlitemId = UtilSql.getValue(result, "c_glitem_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.cGlitemIdr = UtilSql.getValue(result, "c_glitem_idr");
        objectDepositos1CA823322A874B879DEAA2577476814AData.cProjectId = UtilSql.getValue(result, "c_project_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.dateacct = UtilSql.getDateValue(result, "dateacct", "dd-MM-yyyy");
        objectDepositos1CA823322A874B879DEAA2577476814AData.foreignAmount = UtilSql.getValue(result, "foreign_amount");
        objectDepositos1CA823322A874B879DEAA2577476814AData.finReconciliationId = UtilSql.getValue(result, "fin_reconciliation_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.finReconciliationIdr = UtilSql.getValue(result, "fin_reconciliation_idr");
        objectDepositos1CA823322A874B879DEAA2577476814AData.foreignConvertRate = UtilSql.getValue(result, "foreign_convert_rate");
        objectDepositos1CA823322A874B879DEAA2577476814AData.createdbyalgorithm = UtilSql.getValue(result, "createdbyalgorithm");
        objectDepositos1CA823322A874B879DEAA2577476814AData.deleteBtn = UtilSql.getValue(result, "delete_btn");
        objectDepositos1CA823322A874B879DEAA2577476814AData.foreignCurrencyId = UtilSql.getValue(result, "foreign_currency_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.cSalesregionId = UtilSql.getValue(result, "c_salesregion_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.paymentdocno = UtilSql.getValue(result, "paymentdocno");
        objectDepositos1CA823322A874B879DEAA2577476814AData.forcedTableId = UtilSql.getValue(result, "forced_table_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.dpFinaccTransactionVId = UtilSql.getValue(result, "dp_finacc_transaction_v_id");
        objectDepositos1CA823322A874B879DEAA2577476814AData.language = UtilSql.getValue(result, "language");
        objectDepositos1CA823322A874B879DEAA2577476814AData.adUserClient = "";
        objectDepositos1CA823322A874B879DEAA2577476814AData.adOrgClient = "";
        objectDepositos1CA823322A874B879DEAA2577476814AData.createdby = "";
        objectDepositos1CA823322A874B879DEAA2577476814AData.trBgcolor = "";
        objectDepositos1CA823322A874B879DEAA2577476814AData.totalCount = "";
        objectDepositos1CA823322A874B879DEAA2577476814AData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectDepositos1CA823322A874B879DEAA2577476814AData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Depositos1CA823322A874B879DEAA2577476814AData objectDepositos1CA823322A874B879DEAA2577476814AData[] = new Depositos1CA823322A874B879DEAA2577476814AData[vector.size()];
    vector.copyInto(objectDepositos1CA823322A874B879DEAA2577476814AData);
    return(objectDepositos1CA823322A874B879DEAA2577476814AData);
  }

/**
Create a registry
 */
  public static Depositos1CA823322A874B879DEAA2577476814AData[] set(String finFinancialAccountId, String deleteBtn, String finFinaccTransactionId, String cBpartnerId, String trxtype, String dpFinaccTransactionVId, String user1Id, String line, String foreignConvertRate, String processed, String description, String cleared, String depositamt, String reconciled, String createdby, String createdbyr, String cCampaignId, String user2Id, String paymentdocno, String createdbyalgorithm, String cGlitemId, String adClientId, String cCurrencyId, String cActivityId, String cProjectId, String foreignAmount, String adOrgId, String cSalesregionId, String mProductId, String finPaymentId, String paymentamt, String dateacct, String statementdate, String emAprmModify, String isactive, String processing, String status, String emDpDeposito, String foreignCurrencyId, String updatedby, String updatedbyr, String finReconciliationId, String forcedTableId)    throws ServletException {
    Depositos1CA823322A874B879DEAA2577476814AData objectDepositos1CA823322A874B879DEAA2577476814AData[] = new Depositos1CA823322A874B879DEAA2577476814AData[1];
    objectDepositos1CA823322A874B879DEAA2577476814AData[0] = new Depositos1CA823322A874B879DEAA2577476814AData();
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].created = "";
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].createdbyr = createdbyr;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].updated = "";
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].updatedTimeStamp = "";
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].updatedby = updatedby;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].updatedbyr = updatedbyr;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].adOrgId = adOrgId;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].line = line;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].statementdate = statementdate;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].emDpDeposito = emDpDeposito;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].finPaymentId = finPaymentId;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].finPaymentIdr = "";
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].emAprmModify = emAprmModify;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].finFinaccTransactionId = finFinaccTransactionId;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].mProductId = mProductId;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].cBpartnerId = cBpartnerId;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].isactive = isactive;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].status = status;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].statusr = "";
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].finFinancialAccountId = finFinancialAccountId;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].depositamt = depositamt;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].paymentamt = paymentamt;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].processed = processed;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].processing = processing;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].cleared = cleared;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].reconciled = reconciled;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].user1Id = user1Id;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].user2Id = user2Id;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].cCurrencyId = cCurrencyId;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].cCurrencyIdr = "";
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].cActivityId = cActivityId;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].trxtype = trxtype;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].trxtyper = "";
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].description = description;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].cCampaignId = cCampaignId;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].cGlitemId = cGlitemId;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].cGlitemIdr = "";
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].cProjectId = cProjectId;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].dateacct = dateacct;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].foreignAmount = foreignAmount;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].finReconciliationId = finReconciliationId;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].finReconciliationIdr = "";
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].foreignConvertRate = foreignConvertRate;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].createdbyalgorithm = createdbyalgorithm;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].deleteBtn = deleteBtn;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].foreignCurrencyId = foreignCurrencyId;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].cSalesregionId = cSalesregionId;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].paymentdocno = paymentdocno;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].forcedTableId = forcedTableId;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].adClientId = adClientId;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].dpFinaccTransactionVId = dpFinaccTransactionVId;
    objectDepositos1CA823322A874B879DEAA2577476814AData[0].language = "";
    return objectDepositos1CA823322A874B879DEAA2577476814AData;
  }

/**
Select for auxiliar field
 */
  public static String selectActP15C8708DFC464C2D91286E59624FDD18_C_GLItem_ID(ConnectionProvider connectionProvider, String C_GLItem_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT name FROM C_GLItem WHERE C_GLItem_ID=? ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_GLItem_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef1A5CBDE12E734EDCADF7A4BD2CF1AB59(ConnectionProvider connectionProvider, String FIN_Financial_Account_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT COALESCE(MAX(LINE),0)+10 AS DefaultValue FROM FIN_FINACC_TRANSACTION WHERE FIN_Financial_Account_ID=? ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, FIN_Financial_Account_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef55190A56573F4E78B3C883E11F953DFE_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefEFF18BE2A77442C290039A56F8D1A5F2_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT dp_finacc_transaction_v.FIN_Financial_Account_ID AS NAME" +
      "        FROM dp_finacc_transaction_v" +
      "        WHERE dp_finacc_transaction_v.dp_finacc_transaction_v_id = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String finFinancialAccountId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Name), '')) || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table2.ISO_Code), ''))) AS NAME FROM FIN_Financial_Account left join (select FIN_Financial_Account_ID, Name, C_Currency_ID from FIN_Financial_Account) table1 on (FIN_Financial_Account.FIN_Financial_Account_ID = table1.FIN_Financial_Account_ID) left join (select C_Currency_ID, ISO_Code from C_Currency) table2 on (table1.C_Currency_ID = table2.C_Currency_ID) WHERE FIN_Financial_Account.FIN_Financial_Account_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String finFinancialAccountId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Name), '')) || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table2.ISO_Code), ''))) AS NAME FROM FIN_Financial_Account left join (select FIN_Financial_Account_ID, Name, C_Currency_ID from FIN_Financial_Account) table1 on (FIN_Financial_Account.FIN_Financial_Account_ID = table1.FIN_Financial_Account_ID) left join (select C_Currency_ID, ISO_Code from C_Currency) table2 on (table1.C_Currency_ID = table2.C_Currency_ID) WHERE FIN_Financial_Account.FIN_Financial_Account_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE dp_finacc_transaction_v" +
      "        SET AD_Org_ID = (?) , Line = TO_NUMBER(?) , Statementdate = TO_DATE(?) , EM_Dp_Deposito = (?) , FIN_Payment_ID = (?) , EM_Aprm_Modify = (?) , FIN_Finacc_Transaction_ID = (?) , M_Product_ID = (?) , C_Bpartner_ID = (?) , Isactive = (?) , Status = (?) , FIN_Financial_Account_ID = (?) , Depositamt = TO_NUMBER(?) , Paymentamt = TO_NUMBER(?) , Processed = (?) , Processing = (?) , Cleared = (?) , Reconciled = (?) , User1_ID = (?) , User2_ID = (?) , C_Currency_ID = (?) , C_Activity_ID = (?) , Trxtype = (?) , Description = (?) , C_Campaign_ID = (?) , C_Glitem_ID = (?) , C_Project_ID = (?) , Dateacct = TO_DATE(?) , Foreign_Amount = TO_NUMBER(?) , FIN_Reconciliation_ID = (?) , Foreign_Convert_Rate = TO_NUMBER(?) , Createdbyalgorithm = (?) , Delete_Btn = (?) , Foreign_Currency_ID = (?) , C_Salesregion_ID = (?) , Paymentdocno = (?) , Forced_Table_ID = (?) , AD_Client_ID = (?) , dp_finacc_transaction_v_id = (?) , updated = now(), updatedby = ? " +
      "        WHERE dp_finacc_transaction_v.dp_finacc_transaction_v_id = ? " +
      "                 AND dp_finacc_transaction_v.FIN_Financial_Account_ID = ? " +
      "        AND dp_finacc_transaction_v.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND dp_finacc_transaction_v.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, line);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, statementdate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDpDeposito);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAprmModify);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinaccTransactionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, status);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, depositamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paymentamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cleared);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, reconciled);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cActivityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, trxtype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCampaignId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cGlitemId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, foreignAmount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finReconciliationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, foreignConvertRate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdbyalgorithm);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deleteBtn);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, foreignCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cSalesregionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paymentdocno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, forcedTableId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dpFinaccTransactionVId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dpFinaccTransactionVId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO dp_finacc_transaction_v " +
      "        (AD_Org_ID, Line, Statementdate, EM_Dp_Deposito, FIN_Payment_ID, EM_Aprm_Modify, FIN_Finacc_Transaction_ID, M_Product_ID, C_Bpartner_ID, Isactive, Status, FIN_Financial_Account_ID, Depositamt, Paymentamt, Processed, Processing, Cleared, Reconciled, User1_ID, User2_ID, C_Currency_ID, C_Activity_ID, Trxtype, Description, C_Campaign_ID, C_Glitem_ID, C_Project_ID, Dateacct, Foreign_Amount, FIN_Reconciliation_ID, Foreign_Convert_Rate, Createdbyalgorithm, Delete_Btn, Foreign_Currency_ID, C_Salesregion_ID, Paymentdocno, Forced_Table_ID, AD_Client_ID, dp_finacc_transaction_v_id, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), TO_NUMBER(?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), TO_NUMBER(?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, line);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, statementdate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emDpDeposito);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAprmModify);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinaccTransactionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, status);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, depositamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paymentamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cleared);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, reconciled);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cActivityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, trxtype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCampaignId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cGlitemId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, foreignAmount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finReconciliationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, foreignConvertRate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdbyalgorithm);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deleteBtn);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, foreignCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cSalesregionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paymentdocno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, forcedTableId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dpFinaccTransactionVId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String finFinancialAccountId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM dp_finacc_transaction_v" +
      "        WHERE dp_finacc_transaction_v.dp_finacc_transaction_v_id = ? " +
      "                 AND dp_finacc_transaction_v.FIN_Financial_Account_ID = ? " +
      "        AND dp_finacc_transaction_v.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND dp_finacc_transaction_v.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM dp_finacc_transaction_v" +
      "         WHERE dp_finacc_transaction_v.dp_finacc_transaction_v_id = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM dp_finacc_transaction_v" +
      "         WHERE dp_finacc_transaction_v.dp_finacc_transaction_v_id = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
