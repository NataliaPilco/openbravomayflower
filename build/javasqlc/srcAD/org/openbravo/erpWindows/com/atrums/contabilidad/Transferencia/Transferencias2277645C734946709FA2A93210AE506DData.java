//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.contabilidad.Transferencia;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class Transferencias2277645C734946709FA2A93210AE506DData implements FieldProvider {
static Logger log4j = Logger.getLogger(Transferencias2277645C734946709FA2A93210AE506DData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String isactive;
  public String monto;
  public String finPaymentmethodId;
  public String finPaymentmethodIdr;
  public String fechaTransferencia;
  public String cGlitemId;
  public String cGlitemIdr;
  public String bancoRetiro;
  public String bancoRetiror;
  public String adOrgIdPago;
  public String adOrgIdPagor;
  public String pagoCDoctypeId;
  public String pagoCDoctypeIdr;
  public String pagoGenerado;
  public String bancoDeposito;
  public String bancoDepositor;
  public String adOrgIdCobro;
  public String adOrgIdCobror;
  public String cobroCDoctypeId;
  public String cobroCDoctypeIdr;
  public String cobroGenerado;
  public String concepto;
  public String docstatus;
  public String docstatusr;
  public String btnproceso;
  public String btnprocesoBtn;
  public String adOrgId;
  public String coTransferenciaId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("monto"))
      return monto;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_id") || fieldName.equals("finPaymentmethodId"))
      return finPaymentmethodId;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_idr") || fieldName.equals("finPaymentmethodIdr"))
      return finPaymentmethodIdr;
    else if (fieldName.equalsIgnoreCase("fecha_transferencia") || fieldName.equals("fechaTransferencia"))
      return fechaTransferencia;
    else if (fieldName.equalsIgnoreCase("c_glitem_id") || fieldName.equals("cGlitemId"))
      return cGlitemId;
    else if (fieldName.equalsIgnoreCase("c_glitem_idr") || fieldName.equals("cGlitemIdr"))
      return cGlitemIdr;
    else if (fieldName.equalsIgnoreCase("banco_retiro") || fieldName.equals("bancoRetiro"))
      return bancoRetiro;
    else if (fieldName.equalsIgnoreCase("banco_retiror") || fieldName.equals("bancoRetiror"))
      return bancoRetiror;
    else if (fieldName.equalsIgnoreCase("ad_org_id_pago") || fieldName.equals("adOrgIdPago"))
      return adOrgIdPago;
    else if (fieldName.equalsIgnoreCase("ad_org_id_pagor") || fieldName.equals("adOrgIdPagor"))
      return adOrgIdPagor;
    else if (fieldName.equalsIgnoreCase("pago_c_doctype_id") || fieldName.equals("pagoCDoctypeId"))
      return pagoCDoctypeId;
    else if (fieldName.equalsIgnoreCase("pago_c_doctype_idr") || fieldName.equals("pagoCDoctypeIdr"))
      return pagoCDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("pago_generado") || fieldName.equals("pagoGenerado"))
      return pagoGenerado;
    else if (fieldName.equalsIgnoreCase("banco_deposito") || fieldName.equals("bancoDeposito"))
      return bancoDeposito;
    else if (fieldName.equalsIgnoreCase("banco_depositor") || fieldName.equals("bancoDepositor"))
      return bancoDepositor;
    else if (fieldName.equalsIgnoreCase("ad_org_id_cobro") || fieldName.equals("adOrgIdCobro"))
      return adOrgIdCobro;
    else if (fieldName.equalsIgnoreCase("ad_org_id_cobror") || fieldName.equals("adOrgIdCobror"))
      return adOrgIdCobror;
    else if (fieldName.equalsIgnoreCase("cobro_c_doctype_id") || fieldName.equals("cobroCDoctypeId"))
      return cobroCDoctypeId;
    else if (fieldName.equalsIgnoreCase("cobro_c_doctype_idr") || fieldName.equals("cobroCDoctypeIdr"))
      return cobroCDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("cobro_generado") || fieldName.equals("cobroGenerado"))
      return cobroGenerado;
    else if (fieldName.equalsIgnoreCase("concepto"))
      return concepto;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("docstatusr"))
      return docstatusr;
    else if (fieldName.equalsIgnoreCase("btnproceso"))
      return btnproceso;
    else if (fieldName.equalsIgnoreCase("btnproceso_btn") || fieldName.equals("btnprocesoBtn"))
      return btnprocesoBtn;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("co_transferencia_id") || fieldName.equals("coTransferenciaId"))
      return coTransferenciaId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Transferencias2277645C734946709FA2A93210AE506DData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Transferencias2277645C734946709FA2A93210AE506DData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(co_transferencia.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = co_transferencia.CreatedBy) as CreatedByR, " +
      "        to_char(co_transferencia.Updated, ?) as updated, " +
      "        to_char(co_transferencia.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        co_transferencia.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = co_transferencia.UpdatedBy) as UpdatedByR," +
      "        COALESCE(co_transferencia.Isactive, 'N') AS Isactive, " +
      "co_transferencia.Monto, " +
      "co_transferencia.FIN_Paymentmethod_ID, " +
      "(CASE WHEN co_transferencia.FIN_Paymentmethod_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS FIN_Paymentmethod_IDR, " +
      "co_transferencia.Fecha_Transferencia, " +
      "co_transferencia.C_Glitem_ID, " +
      "(CASE WHEN co_transferencia.C_Glitem_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS C_Glitem_IDR, " +
      "co_transferencia.Banco_Retiro, " +
      "(CASE WHEN co_transferencia.Banco_Retiro IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Name), ''))),'') ) END) AS Banco_RetiroR, " +
      "co_transferencia.ad_org_id_pago, " +
      "(CASE WHEN co_transferencia.ad_org_id_pago IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS ad_org_id_pagoR, " +
      "co_transferencia.Pago_C_Doctype_ID, " +
      "(CASE WHEN co_transferencia.Pago_C_Doctype_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL5.Name IS NULL THEN TO_CHAR(table5.Name) ELSE TO_CHAR(tableTRL5.Name) END)), ''))),'') ) END) AS Pago_C_Doctype_IDR, " +
      "co_transferencia.Pago_Generado, " +
      "co_transferencia.Banco_Deposito, " +
      "(CASE WHEN co_transferencia.Banco_Deposito IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table7.Name), ''))),'') ) END) AS Banco_DepositoR, " +
      "co_transferencia.ad_org_id_cobro, " +
      "(CASE WHEN co_transferencia.ad_org_id_cobro IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table8.Name), ''))),'') ) END) AS ad_org_id_cobroR, " +
      "co_transferencia.Cobro_C_Doctype_ID, " +
      "(CASE WHEN co_transferencia.Cobro_C_Doctype_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL9.Name IS NULL THEN TO_CHAR(table9.Name) ELSE TO_CHAR(tableTRL9.Name) END)), ''))),'') ) END) AS Cobro_C_Doctype_IDR, " +
      "co_transferencia.Cobro_Generado, " +
      "co_transferencia.Concepto, " +
      "co_transferencia.Docstatus, " +
      "(CASE WHEN co_transferencia.Docstatus IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS DocstatusR, " +
      "co_transferencia.Btnproceso, " +
      "list2.name as Btnproceso_BTN, " +
      "co_transferencia.AD_Org_ID, " +
      "co_transferencia.CO_Transferencia_ID, " +
      "co_transferencia.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM co_transferencia left join (select Fin_Paymentmethod_ID, Name from FIN_PaymentMethod) table1 on (co_transferencia.FIN_Paymentmethod_ID =  table1.Fin_Paymentmethod_ID) left join (select C_Glitem_ID, Name from C_Glitem) table2 on (co_transferencia.C_Glitem_ID =  table2.C_Glitem_ID) left join (select Fin_Financial_Account_ID, Name from FIN_Financial_Account) table3 on (co_transferencia.Banco_Retiro =  table3.Fin_Financial_Account_ID) left join (select AD_Org_ID, Name from AD_Org) table4 on (co_transferencia.ad_org_id_pago =  table4.AD_Org_ID) left join (select C_DocType_ID, Name from C_DocType) table5 on (co_transferencia.Pago_C_Doctype_ID =  table5.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL5 on (table5.C_DocType_ID = tableTRL5.C_DocType_ID and tableTRL5.AD_Language = ?)  left join (select Fin_Financial_Account_ID, Name from FIN_Financial_Account) table7 on (co_transferencia.Banco_Deposito =  table7.Fin_Financial_Account_ID) left join (select AD_Org_ID, Name from AD_Org) table8 on (co_transferencia.ad_org_id_cobro =  table8.AD_Org_ID) left join (select C_DocType_ID, Name from C_DocType) table9 on (co_transferencia.Cobro_C_Doctype_ID =  table9.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL9 on (table9.C_DocType_ID = tableTRL9.C_DocType_ID and tableTRL9.AD_Language = ?)  left join ad_ref_list_v list1 on (co_transferencia.Docstatus = list1.value and list1.ad_reference_id = 'D614699DC7B84E528A35AB78F9DCF312' and list1.ad_language = ?)  left join ad_ref_list_v list2 on (list2.ad_reference_id = 'D614699DC7B84E528A35AB78F9DCF312' and list2.ad_language = ?  AND co_transferencia.Btnproceso = TO_CHAR(list2.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND co_transferencia.CO_Transferencia_ID = ? " +
      "        AND co_transferencia.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND co_transferencia.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Transferencias2277645C734946709FA2A93210AE506DData objectTransferencias2277645C734946709FA2A93210AE506DData = new Transferencias2277645C734946709FA2A93210AE506DData();
        objectTransferencias2277645C734946709FA2A93210AE506DData.created = UtilSql.getValue(result, "created");
        objectTransferencias2277645C734946709FA2A93210AE506DData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectTransferencias2277645C734946709FA2A93210AE506DData.updated = UtilSql.getValue(result, "updated");
        objectTransferencias2277645C734946709FA2A93210AE506DData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectTransferencias2277645C734946709FA2A93210AE506DData.updatedby = UtilSql.getValue(result, "updatedby");
        objectTransferencias2277645C734946709FA2A93210AE506DData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectTransferencias2277645C734946709FA2A93210AE506DData.isactive = UtilSql.getValue(result, "isactive");
        objectTransferencias2277645C734946709FA2A93210AE506DData.monto = UtilSql.getValue(result, "monto");
        objectTransferencias2277645C734946709FA2A93210AE506DData.finPaymentmethodId = UtilSql.getValue(result, "fin_paymentmethod_id");
        objectTransferencias2277645C734946709FA2A93210AE506DData.finPaymentmethodIdr = UtilSql.getValue(result, "fin_paymentmethod_idr");
        objectTransferencias2277645C734946709FA2A93210AE506DData.fechaTransferencia = UtilSql.getDateValue(result, "fecha_transferencia", "dd-MM-yyyy");
        objectTransferencias2277645C734946709FA2A93210AE506DData.cGlitemId = UtilSql.getValue(result, "c_glitem_id");
        objectTransferencias2277645C734946709FA2A93210AE506DData.cGlitemIdr = UtilSql.getValue(result, "c_glitem_idr");
        objectTransferencias2277645C734946709FA2A93210AE506DData.bancoRetiro = UtilSql.getValue(result, "banco_retiro");
        objectTransferencias2277645C734946709FA2A93210AE506DData.bancoRetiror = UtilSql.getValue(result, "banco_retiror");
        objectTransferencias2277645C734946709FA2A93210AE506DData.adOrgIdPago = UtilSql.getValue(result, "ad_org_id_pago");
        objectTransferencias2277645C734946709FA2A93210AE506DData.adOrgIdPagor = UtilSql.getValue(result, "ad_org_id_pagor");
        objectTransferencias2277645C734946709FA2A93210AE506DData.pagoCDoctypeId = UtilSql.getValue(result, "pago_c_doctype_id");
        objectTransferencias2277645C734946709FA2A93210AE506DData.pagoCDoctypeIdr = UtilSql.getValue(result, "pago_c_doctype_idr");
        objectTransferencias2277645C734946709FA2A93210AE506DData.pagoGenerado = UtilSql.getValue(result, "pago_generado");
        objectTransferencias2277645C734946709FA2A93210AE506DData.bancoDeposito = UtilSql.getValue(result, "banco_deposito");
        objectTransferencias2277645C734946709FA2A93210AE506DData.bancoDepositor = UtilSql.getValue(result, "banco_depositor");
        objectTransferencias2277645C734946709FA2A93210AE506DData.adOrgIdCobro = UtilSql.getValue(result, "ad_org_id_cobro");
        objectTransferencias2277645C734946709FA2A93210AE506DData.adOrgIdCobror = UtilSql.getValue(result, "ad_org_id_cobror");
        objectTransferencias2277645C734946709FA2A93210AE506DData.cobroCDoctypeId = UtilSql.getValue(result, "cobro_c_doctype_id");
        objectTransferencias2277645C734946709FA2A93210AE506DData.cobroCDoctypeIdr = UtilSql.getValue(result, "cobro_c_doctype_idr");
        objectTransferencias2277645C734946709FA2A93210AE506DData.cobroGenerado = UtilSql.getValue(result, "cobro_generado");
        objectTransferencias2277645C734946709FA2A93210AE506DData.concepto = UtilSql.getValue(result, "concepto");
        objectTransferencias2277645C734946709FA2A93210AE506DData.docstatus = UtilSql.getValue(result, "docstatus");
        objectTransferencias2277645C734946709FA2A93210AE506DData.docstatusr = UtilSql.getValue(result, "docstatusr");
        objectTransferencias2277645C734946709FA2A93210AE506DData.btnproceso = UtilSql.getValue(result, "btnproceso");
        objectTransferencias2277645C734946709FA2A93210AE506DData.btnprocesoBtn = UtilSql.getValue(result, "btnproceso_btn");
        objectTransferencias2277645C734946709FA2A93210AE506DData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectTransferencias2277645C734946709FA2A93210AE506DData.coTransferenciaId = UtilSql.getValue(result, "co_transferencia_id");
        objectTransferencias2277645C734946709FA2A93210AE506DData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectTransferencias2277645C734946709FA2A93210AE506DData.language = UtilSql.getValue(result, "language");
        objectTransferencias2277645C734946709FA2A93210AE506DData.adUserClient = "";
        objectTransferencias2277645C734946709FA2A93210AE506DData.adOrgClient = "";
        objectTransferencias2277645C734946709FA2A93210AE506DData.createdby = "";
        objectTransferencias2277645C734946709FA2A93210AE506DData.trBgcolor = "";
        objectTransferencias2277645C734946709FA2A93210AE506DData.totalCount = "";
        objectTransferencias2277645C734946709FA2A93210AE506DData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectTransferencias2277645C734946709FA2A93210AE506DData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Transferencias2277645C734946709FA2A93210AE506DData objectTransferencias2277645C734946709FA2A93210AE506DData[] = new Transferencias2277645C734946709FA2A93210AE506DData[vector.size()];
    vector.copyInto(objectTransferencias2277645C734946709FA2A93210AE506DData);
    return(objectTransferencias2277645C734946709FA2A93210AE506DData);
  }

/**
Create a registry
 */
  public static Transferencias2277645C734946709FA2A93210AE506DData[] set(String adOrgIdCobro, String cobroCDoctypeId, String pagoCDoctypeId, String cobroGenerado, String cGlitemId, String adOrgId, String createdby, String createdbyr, String fechaTransferencia, String updatedby, String updatedbyr, String adClientId, String concepto, String bancoRetiro, String pagoGenerado, String bancoDeposito, String docstatus, String monto, String coTransferenciaId, String finPaymentmethodId, String isactive, String btnproceso, String btnprocesoBtn, String adOrgIdPago)    throws ServletException {
    Transferencias2277645C734946709FA2A93210AE506DData objectTransferencias2277645C734946709FA2A93210AE506DData[] = new Transferencias2277645C734946709FA2A93210AE506DData[1];
    objectTransferencias2277645C734946709FA2A93210AE506DData[0] = new Transferencias2277645C734946709FA2A93210AE506DData();
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].created = "";
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].createdbyr = createdbyr;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].updated = "";
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].updatedTimeStamp = "";
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].updatedby = updatedby;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].updatedbyr = updatedbyr;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].isactive = isactive;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].monto = monto;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].finPaymentmethodId = finPaymentmethodId;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].finPaymentmethodIdr = "";
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].fechaTransferencia = fechaTransferencia;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].cGlitemId = cGlitemId;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].cGlitemIdr = "";
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].bancoRetiro = bancoRetiro;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].bancoRetiror = "";
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].adOrgIdPago = adOrgIdPago;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].adOrgIdPagor = "";
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].pagoCDoctypeId = pagoCDoctypeId;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].pagoCDoctypeIdr = "";
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].pagoGenerado = pagoGenerado;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].bancoDeposito = bancoDeposito;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].bancoDepositor = "";
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].adOrgIdCobro = adOrgIdCobro;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].adOrgIdCobror = "";
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].cobroCDoctypeId = cobroCDoctypeId;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].cobroCDoctypeIdr = "";
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].cobroGenerado = cobroGenerado;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].concepto = concepto;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].docstatus = docstatus;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].docstatusr = "";
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].btnproceso = btnproceso;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].btnprocesoBtn = btnprocesoBtn;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].adOrgId = adOrgId;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].coTransferenciaId = coTransferenciaId;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].adClientId = adClientId;
    objectTransferencias2277645C734946709FA2A93210AE506DData[0].language = "";
    return objectTransferencias2277645C734946709FA2A93210AE506DData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef446DA057CAEC4F648CD32049DE459DD9_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef4D1CADAA279B4C9F906030869A7A5B83_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE co_transferencia" +
      "        SET Isactive = (?) , Monto = TO_NUMBER(?) , FIN_Paymentmethod_ID = (?) , Fecha_Transferencia = TO_DATE(?) , C_Glitem_ID = (?) , Banco_Retiro = (?) , ad_org_id_pago = (?) , Pago_C_Doctype_ID = (?) , Pago_Generado = (?) , Banco_Deposito = (?) , ad_org_id_cobro = (?) , Cobro_C_Doctype_ID = (?) , Cobro_Generado = (?) , Concepto = (?) , Docstatus = (?) , Btnproceso = (?) , AD_Org_ID = (?) , CO_Transferencia_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE co_transferencia.CO_Transferencia_ID = ? " +
      "        AND co_transferencia.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND co_transferencia.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, monto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaTransferencia);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cGlitemId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bancoRetiro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgIdPago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pagoCDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pagoGenerado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bancoDeposito);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgIdCobro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cobroCDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cobroGenerado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, concepto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, btnproceso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coTransferenciaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coTransferenciaId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO co_transferencia " +
      "        (Isactive, Monto, FIN_Paymentmethod_ID, Fecha_Transferencia, C_Glitem_ID, Banco_Retiro, ad_org_id_pago, Pago_C_Doctype_ID, Pago_Generado, Banco_Deposito, ad_org_id_cobro, Cobro_C_Doctype_ID, Cobro_Generado, Concepto, Docstatus, Btnproceso, AD_Org_ID, CO_Transferencia_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), TO_NUMBER(?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, monto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaTransferencia);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cGlitemId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bancoRetiro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgIdPago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pagoCDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pagoGenerado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bancoDeposito);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgIdCobro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cobroCDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cobroGenerado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, concepto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, btnproceso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coTransferenciaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM co_transferencia" +
      "        WHERE co_transferencia.CO_Transferencia_ID = ? " +
      "        AND co_transferencia.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND co_transferencia.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM co_transferencia" +
      "         WHERE co_transferencia.CO_Transferencia_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM co_transferencia" +
      "         WHERE co_transferencia.CO_Transferencia_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
