//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.SalarioDigno;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class SalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData implements FieldProvider {
static Logger log4j = Logger.getLogger(SalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String cYearId;
  public String cYearIdr;
  public String salarioDignoMensual;
  public String descripcion;
  public String processed;
  public String docstatus;
  public String docaction;
  public String generaDatos;
  public String generaCsv;
  public String isactive;
  public String noSalarioDignoId;
  public String adOrgId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("c_year_id") || fieldName.equals("cYearId"))
      return cYearId;
    else if (fieldName.equalsIgnoreCase("c_year_idr") || fieldName.equals("cYearIdr"))
      return cYearIdr;
    else if (fieldName.equalsIgnoreCase("salario_digno_mensual") || fieldName.equals("salarioDignoMensual"))
      return salarioDignoMensual;
    else if (fieldName.equalsIgnoreCase("descripcion"))
      return descripcion;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("docaction"))
      return docaction;
    else if (fieldName.equalsIgnoreCase("genera_datos") || fieldName.equals("generaDatos"))
      return generaDatos;
    else if (fieldName.equalsIgnoreCase("genera_csv") || fieldName.equals("generaCsv"))
      return generaCsv;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("no_salario_digno_id") || fieldName.equals("noSalarioDignoId"))
      return noSalarioDignoId;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static SalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static SalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(no_salario_digno.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_salario_digno.CreatedBy) as CreatedByR, " +
      "        to_char(no_salario_digno.Updated, ?) as updated, " +
      "        to_char(no_salario_digno.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        no_salario_digno.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_salario_digno.UpdatedBy) as UpdatedByR," +
      "        no_salario_digno.C_Year_ID, " +
      "(CASE WHEN no_salario_digno.C_Year_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Year), ''))),'') ) END) AS C_Year_IDR, " +
      "no_salario_digno.Salario_Digno_Mensual, " +
      "no_salario_digno.Descripcion, " +
      "COALESCE(no_salario_digno.Processed, 'N') AS Processed, " +
      "no_salario_digno.Docstatus, " +
      "no_salario_digno.DocAction, " +
      "no_salario_digno.Genera_Datos, " +
      "no_salario_digno.Genera_Csv, " +
      "COALESCE(no_salario_digno.Isactive, 'N') AS Isactive, " +
      "no_salario_digno.NO_Salario_Digno_ID, " +
      "no_salario_digno.AD_Org_ID, " +
      "no_salario_digno.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM no_salario_digno left join (select C_Year_ID, Year from C_Year) table1 on (no_salario_digno.C_Year_ID = table1.C_Year_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND no_salario_digno.NO_Salario_Digno_ID = ? " +
      "        AND no_salario_digno.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND no_salario_digno.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        SalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData = new SalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData();
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.created = UtilSql.getValue(result, "created");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.updated = UtilSql.getValue(result, "updated");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.updatedby = UtilSql.getValue(result, "updatedby");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.cYearId = UtilSql.getValue(result, "c_year_id");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.cYearIdr = UtilSql.getValue(result, "c_year_idr");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.salarioDignoMensual = UtilSql.getValue(result, "salario_digno_mensual");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.descripcion = UtilSql.getValue(result, "descripcion");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.processed = UtilSql.getValue(result, "processed");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.docstatus = UtilSql.getValue(result, "docstatus");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.docaction = UtilSql.getValue(result, "docaction");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.generaDatos = UtilSql.getValue(result, "genera_datos");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.generaCsv = UtilSql.getValue(result, "genera_csv");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.isactive = UtilSql.getValue(result, "isactive");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.noSalarioDignoId = UtilSql.getValue(result, "no_salario_digno_id");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.language = UtilSql.getValue(result, "language");
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.adUserClient = "";
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.adOrgClient = "";
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.createdby = "";
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.trBgcolor = "";
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.totalCount = "";
        objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    SalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[] = new SalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[vector.size()];
    vector.copyInto(objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData);
    return(objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData);
  }

/**
Create a registry
 */
  public static SalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[] set(String processed, String cYearId, String descripcion, String adOrgId, String adClientId, String createdby, String createdbyr, String salarioDignoMensual, String docstatus, String updatedby, String updatedbyr, String generaCsv, String generaDatos, String isactive, String docaction, String noSalarioDignoId)    throws ServletException {
    SalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[] = new SalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[1];
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0] = new SalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData();
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].created = "";
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].createdbyr = createdbyr;
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].updated = "";
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].updatedTimeStamp = "";
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].updatedby = updatedby;
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].updatedbyr = updatedbyr;
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].cYearId = cYearId;
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].cYearIdr = "";
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].salarioDignoMensual = salarioDignoMensual;
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].descripcion = descripcion;
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].processed = processed;
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].docstatus = docstatus;
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].docaction = docaction;
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].generaDatos = generaDatos;
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].generaCsv = generaCsv;
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].isactive = isactive;
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].noSalarioDignoId = noSalarioDignoId;
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].adOrgId = adOrgId;
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].adClientId = adClientId;
    objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData[0].language = "";
    return objectSalarioDigno910392DFB0E94D9CB9B160A6BCDC4DBFData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef4A2E6621D85A4BF99F8EC3EFDF6A93A3_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef852A7532EC3841C587B6787D8C814579_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE no_salario_digno" +
      "        SET C_Year_ID = (?) , Salario_Digno_Mensual = TO_NUMBER(?) , Descripcion = (?) , Processed = (?) , Docstatus = (?) , DocAction = (?) , Genera_Datos = (?) , Genera_Csv = (?) , Isactive = (?) , NO_Salario_Digno_ID = (?) , AD_Org_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE no_salario_digno.NO_Salario_Digno_ID = ? " +
      "        AND no_salario_digno.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_salario_digno.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cYearId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salarioDignoMensual);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generaDatos);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generaCsv);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noSalarioDignoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noSalarioDignoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO no_salario_digno " +
      "        (C_Year_ID, Salario_Digno_Mensual, Descripcion, Processed, Docstatus, DocAction, Genera_Datos, Genera_Csv, Isactive, NO_Salario_Digno_ID, AD_Org_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cYearId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salarioDignoMensual);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generaDatos);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generaCsv);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noSalarioDignoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM no_salario_digno" +
      "        WHERE no_salario_digno.NO_Salario_Digno_ID = ? " +
      "        AND no_salario_digno.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_salario_digno.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM no_salario_digno" +
      "         WHERE no_salario_digno.NO_Salario_Digno_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM no_salario_digno" +
      "         WHERE no_salario_digno.NO_Salario_Digno_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
