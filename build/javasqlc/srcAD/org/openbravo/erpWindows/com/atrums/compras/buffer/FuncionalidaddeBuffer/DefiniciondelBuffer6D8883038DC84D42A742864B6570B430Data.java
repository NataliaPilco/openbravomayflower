//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.compras.buffer.FuncionalidaddeBuffer;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class DefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data implements FieldProvider {
static Logger log4j = Logger.getLogger(DefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String mWarehouseId;
  public String mWarehouseIdr;
  public String nombre;
  public String tipoDocumento;
  public String tipoDocumentor;
  public String mesPromedio;
  public String isactive;
  public String processed;
  public String couParamBufferId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("m_warehouse_id") || fieldName.equals("mWarehouseId"))
      return mWarehouseId;
    else if (fieldName.equalsIgnoreCase("m_warehouse_idr") || fieldName.equals("mWarehouseIdr"))
      return mWarehouseIdr;
    else if (fieldName.equalsIgnoreCase("nombre"))
      return nombre;
    else if (fieldName.equalsIgnoreCase("tipo_documento") || fieldName.equals("tipoDocumento"))
      return tipoDocumento;
    else if (fieldName.equalsIgnoreCase("tipo_documentor") || fieldName.equals("tipoDocumentor"))
      return tipoDocumentor;
    else if (fieldName.equalsIgnoreCase("mes_promedio") || fieldName.equals("mesPromedio"))
      return mesPromedio;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("cou_param_buffer_id") || fieldName.equals("couParamBufferId"))
      return couParamBufferId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static DefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static DefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(cou_param_buffer.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = cou_param_buffer.CreatedBy) as CreatedByR, " +
      "        to_char(cou_param_buffer.Updated, ?) as updated, " +
      "        to_char(cou_param_buffer.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        cou_param_buffer.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = cou_param_buffer.UpdatedBy) as UpdatedByR," +
      "        cou_param_buffer.AD_Org_ID, " +
      "(CASE WHEN cou_param_buffer.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "cou_param_buffer.M_Warehouse_ID, " +
      "(CASE WHEN cou_param_buffer.M_Warehouse_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS M_Warehouse_IDR, " +
      "cou_param_buffer.Nombre, " +
      "cou_param_buffer.Tipo_Documento, " +
      "(CASE WHEN cou_param_buffer.Tipo_Documento IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS Tipo_DocumentoR, " +
      "cou_param_buffer.MES_Promedio, " +
      "COALESCE(cou_param_buffer.Isactive, 'N') AS Isactive, " +
      "cou_param_buffer.Processed, " +
      "cou_param_buffer.COU_Param_Buffer_ID, " +
      "cou_param_buffer.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM cou_param_buffer left join (select AD_Org_ID, Name from AD_Org) table1 on (cou_param_buffer.AD_Org_ID = table1.AD_Org_ID) left join (select M_Warehouse_ID, Name from M_Warehouse) table2 on (cou_param_buffer.M_Warehouse_ID = table2.M_Warehouse_ID) left join ad_ref_list_v list1 on (cou_param_buffer.Tipo_Documento = list1.value and list1.ad_reference_id = '7F16E35E98544100B5128AC166E01B93' and list1.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND cou_param_buffer.COU_Param_Buffer_ID = ? " +
      "        AND cou_param_buffer.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND cou_param_buffer.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        DefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data = new DefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data();
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.created = UtilSql.getValue(result, "created");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.updated = UtilSql.getValue(result, "updated");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.mWarehouseId = UtilSql.getValue(result, "m_warehouse_id");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.mWarehouseIdr = UtilSql.getValue(result, "m_warehouse_idr");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.nombre = UtilSql.getValue(result, "nombre");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.tipoDocumento = UtilSql.getValue(result, "tipo_documento");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.tipoDocumentor = UtilSql.getValue(result, "tipo_documentor");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.mesPromedio = UtilSql.getValue(result, "mes_promedio");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.isactive = UtilSql.getValue(result, "isactive");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.processed = UtilSql.getValue(result, "processed");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.couParamBufferId = UtilSql.getValue(result, "cou_param_buffer_id");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.language = UtilSql.getValue(result, "language");
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.adUserClient = "";
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.adOrgClient = "";
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.createdby = "";
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.trBgcolor = "";
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.totalCount = "";
        objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    DefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[] = new DefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[vector.size()];
    vector.copyInto(objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data);
    return(objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data);
  }

/**
Create a registry
 */
  public static DefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[] set(String createdby, String createdbyr, String mWarehouseId, String adClientId, String processed, String updatedby, String updatedbyr, String isactive, String couParamBufferId, String mesPromedio, String tipoDocumento, String adOrgId, String nombre)    throws ServletException {
    DefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[] = new DefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[1];
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0] = new DefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data();
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].created = "";
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].createdbyr = createdbyr;
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].updated = "";
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].updatedTimeStamp = "";
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].updatedby = updatedby;
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].updatedbyr = updatedbyr;
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].adOrgId = adOrgId;
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].adOrgIdr = "";
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].mWarehouseId = mWarehouseId;
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].mWarehouseIdr = "";
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].nombre = nombre;
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].tipoDocumento = tipoDocumento;
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].tipoDocumentor = "";
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].mesPromedio = mesPromedio;
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].isactive = isactive;
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].processed = processed;
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].couParamBufferId = couParamBufferId;
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].adClientId = adClientId;
    objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data[0].language = "";
    return objectDefiniciondelBuffer6D8883038DC84D42A742864B6570B430Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef08671EAB1DBD424E84BE52AC2E5BBA8B_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2F56558BCE134DC899F7CC488F655668_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE cou_param_buffer" +
      "        SET AD_Org_ID = (?) , M_Warehouse_ID = (?) , Nombre = (?) , Tipo_Documento = (?) , MES_Promedio = TO_NUMBER(?) , Isactive = (?) , Processed = (?) , COU_Param_Buffer_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE cou_param_buffer.COU_Param_Buffer_ID = ? " +
      "        AND cou_param_buffer.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND cou_param_buffer.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nombre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoDocumento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mesPromedio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, couParamBufferId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, couParamBufferId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO cou_param_buffer " +
      "        (AD_Org_ID, M_Warehouse_ID, Nombre, Tipo_Documento, MES_Promedio, Isactive, Processed, COU_Param_Buffer_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nombre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoDocumento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mesPromedio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, couParamBufferId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM cou_param_buffer" +
      "        WHERE cou_param_buffer.COU_Param_Buffer_ID = ? " +
      "        AND cou_param_buffer.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND cou_param_buffer.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM cou_param_buffer" +
      "         WHERE cou_param_buffer.COU_Param_Buffer_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM cou_param_buffer" +
      "         WHERE cou_param_buffer.COU_Param_Buffer_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
