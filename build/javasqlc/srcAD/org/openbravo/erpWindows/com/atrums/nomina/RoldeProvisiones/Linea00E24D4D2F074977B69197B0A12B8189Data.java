//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.RoldeProvisiones;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class Linea00E24D4D2F074977B69197B0A12B8189Data implements FieldProvider {
static Logger log4j = Logger.getLogger(Linea00E24D4D2F074977B69197B0A12B8189Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String line;
  public String noTipoIngresoEgresoId;
  public String noTipoIngresoEgresoIdr;
  public String fechainicio;
  public String fechafin;
  public String valor;
  public String cCurrencyId;
  public String cCurrencyIdr;
  public String isactive;
  public String docstatus;
  public String cDoctypeId;
  public String documentno;
  public String docaccionno;
  public String docaccionnoBtn;
  public String noRolPagoProvisionLineId;
  public String adOrgId;
  public String adClientId;
  public String noRolPagoProvisionId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("line"))
      return line;
    else if (fieldName.equalsIgnoreCase("no_tipo_ingreso_egreso_id") || fieldName.equals("noTipoIngresoEgresoId"))
      return noTipoIngresoEgresoId;
    else if (fieldName.equalsIgnoreCase("no_tipo_ingreso_egreso_idr") || fieldName.equals("noTipoIngresoEgresoIdr"))
      return noTipoIngresoEgresoIdr;
    else if (fieldName.equalsIgnoreCase("fechainicio"))
      return fechainicio;
    else if (fieldName.equalsIgnoreCase("fechafin"))
      return fechafin;
    else if (fieldName.equalsIgnoreCase("valor"))
      return valor;
    else if (fieldName.equalsIgnoreCase("c_currency_id") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("c_currency_idr") || fieldName.equals("cCurrencyIdr"))
      return cCurrencyIdr;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("docaccionno"))
      return docaccionno;
    else if (fieldName.equalsIgnoreCase("docaccionno_btn") || fieldName.equals("docaccionnoBtn"))
      return docaccionnoBtn;
    else if (fieldName.equalsIgnoreCase("no_rol_pago_provision_line_id") || fieldName.equals("noRolPagoProvisionLineId"))
      return noRolPagoProvisionLineId;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("no_rol_pago_provision_id") || fieldName.equals("noRolPagoProvisionId"))
      return noRolPagoProvisionId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Linea00E24D4D2F074977B69197B0A12B8189Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String noRolPagoProvisionId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, noRolPagoProvisionId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Linea00E24D4D2F074977B69197B0A12B8189Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String noRolPagoProvisionId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(no_rol_pago_provision_line.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_rol_pago_provision_line.CreatedBy) as CreatedByR, " +
      "        to_char(no_rol_pago_provision_line.Updated, ?) as updated, " +
      "        to_char(no_rol_pago_provision_line.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        no_rol_pago_provision_line.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_rol_pago_provision_line.UpdatedBy) as UpdatedByR," +
      "        no_rol_pago_provision_line.Line, " +
      "no_rol_pago_provision_line.NO_Tipo_Ingreso_Egreso_ID, " +
      "(CASE WHEN no_rol_pago_provision_line.NO_Tipo_Ingreso_Egreso_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS NO_Tipo_Ingreso_Egreso_IDR, " +
      "no_rol_pago_provision_line.Fechainicio, " +
      "no_rol_pago_provision_line.Fechafin, " +
      "no_rol_pago_provision_line.Valor, " +
      "no_rol_pago_provision_line.C_Currency_ID, " +
      "(CASE WHEN no_rol_pago_provision_line.C_Currency_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.ISO_Code), ''))),'') ) END) AS C_Currency_IDR, " +
      "COALESCE(no_rol_pago_provision_line.Isactive, 'N') AS Isactive, " +
      "no_rol_pago_provision_line.Docstatus, " +
      "no_rol_pago_provision_line.C_Doctype_ID, " +
      "no_rol_pago_provision_line.Documentno, " +
      "no_rol_pago_provision_line.Docaccionno, " +
      "list1.name as Docaccionno_BTN, " +
      "no_rol_pago_provision_line.NO_Rol_Pago_Provision_Line_ID, " +
      "no_rol_pago_provision_line.AD_Org_ID, " +
      "no_rol_pago_provision_line.AD_Client_ID, " +
      "no_rol_pago_provision_line.NO_Rol_Pago_Provision_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM no_rol_pago_provision_line left join (select NO_Tipo_Ingreso_Egreso_ID, Name from NO_Tipo_Ingreso_Egreso) table1 on (no_rol_pago_provision_line.NO_Tipo_Ingreso_Egreso_ID = table1.NO_Tipo_Ingreso_Egreso_ID) left join (select C_Currency_ID, ISO_Code from C_Currency) table2 on (no_rol_pago_provision_line.C_Currency_ID = table2.C_Currency_ID) left join ad_ref_list_v list1 on (list1.ad_reference_id = '31D050E5C2D843B99AD7E9470D9E8579' and list1.ad_language = ?  AND no_rol_pago_provision_line.Docaccionno = TO_CHAR(list1.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((noRolPagoProvisionId==null || noRolPagoProvisionId.equals(""))?"":"  AND no_rol_pago_provision_line.NO_Rol_Pago_Provision_ID = ?  ");
    strSql = strSql + 
      "        AND no_rol_pago_provision_line.NO_Rol_Pago_Provision_Line_ID = ? " +
      "        AND no_rol_pago_provision_line.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND no_rol_pago_provision_line.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (noRolPagoProvisionId != null && !(noRolPagoProvisionId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Linea00E24D4D2F074977B69197B0A12B8189Data objectLinea00E24D4D2F074977B69197B0A12B8189Data = new Linea00E24D4D2F074977B69197B0A12B8189Data();
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.created = UtilSql.getValue(result, "created");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.updated = UtilSql.getValue(result, "updated");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.line = UtilSql.getValue(result, "line");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.noTipoIngresoEgresoId = UtilSql.getValue(result, "no_tipo_ingreso_egreso_id");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.noTipoIngresoEgresoIdr = UtilSql.getValue(result, "no_tipo_ingreso_egreso_idr");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.fechainicio = UtilSql.getDateValue(result, "fechainicio", "dd-MM-yyyy");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.fechafin = UtilSql.getDateValue(result, "fechafin", "dd-MM-yyyy");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.valor = UtilSql.getValue(result, "valor");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.cCurrencyId = UtilSql.getValue(result, "c_currency_id");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.cCurrencyIdr = UtilSql.getValue(result, "c_currency_idr");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.isactive = UtilSql.getValue(result, "isactive");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.docstatus = UtilSql.getValue(result, "docstatus");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.documentno = UtilSql.getValue(result, "documentno");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.docaccionno = UtilSql.getValue(result, "docaccionno");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.docaccionnoBtn = UtilSql.getValue(result, "docaccionno_btn");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.noRolPagoProvisionLineId = UtilSql.getValue(result, "no_rol_pago_provision_line_id");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.noRolPagoProvisionId = UtilSql.getValue(result, "no_rol_pago_provision_id");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.language = UtilSql.getValue(result, "language");
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.adUserClient = "";
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.adOrgClient = "";
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.createdby = "";
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.trBgcolor = "";
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.totalCount = "";
        objectLinea00E24D4D2F074977B69197B0A12B8189Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLinea00E24D4D2F074977B69197B0A12B8189Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Linea00E24D4D2F074977B69197B0A12B8189Data objectLinea00E24D4D2F074977B69197B0A12B8189Data[] = new Linea00E24D4D2F074977B69197B0A12B8189Data[vector.size()];
    vector.copyInto(objectLinea00E24D4D2F074977B69197B0A12B8189Data);
    return(objectLinea00E24D4D2F074977B69197B0A12B8189Data);
  }

/**
Create a registry
 */
  public static Linea00E24D4D2F074977B69197B0A12B8189Data[] set(String noRolPagoProvisionId, String valor, String documentno, String docstatus, String cDoctypeId, String fechafin, String updatedby, String updatedbyr, String isactive, String docaccionno, String docaccionnoBtn, String noTipoIngresoEgresoId, String cCurrencyId, String adClientId, String noRolPagoProvisionLineId, String fechainicio, String line, String createdby, String createdbyr, String adOrgId)    throws ServletException {
    Linea00E24D4D2F074977B69197B0A12B8189Data objectLinea00E24D4D2F074977B69197B0A12B8189Data[] = new Linea00E24D4D2F074977B69197B0A12B8189Data[1];
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0] = new Linea00E24D4D2F074977B69197B0A12B8189Data();
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].created = "";
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].createdbyr = createdbyr;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].updated = "";
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].updatedTimeStamp = "";
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].updatedby = updatedby;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].updatedbyr = updatedbyr;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].line = line;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].noTipoIngresoEgresoId = noTipoIngresoEgresoId;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].noTipoIngresoEgresoIdr = "";
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].fechainicio = fechainicio;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].fechafin = fechafin;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].valor = valor;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].cCurrencyId = cCurrencyId;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].cCurrencyIdr = "";
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].isactive = isactive;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].docstatus = docstatus;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].cDoctypeId = cDoctypeId;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].documentno = documentno;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].docaccionno = docaccionno;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].docaccionnoBtn = docaccionnoBtn;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].noRolPagoProvisionLineId = noRolPagoProvisionLineId;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].adOrgId = adOrgId;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].adClientId = adClientId;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].noRolPagoProvisionId = noRolPagoProvisionId;
    objectLinea00E24D4D2F074977B69197B0A12B8189Data[0].language = "";
    return objectLinea00E24D4D2F074977B69197B0A12B8189Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef40740A56F8384F0DA75AB6D39B6D410D_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefC75F6017E9C344518FD90992C7DA7F6F(ConnectionProvider connectionProvider, String NO_Rol_Pago_Provision_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT COALESCE(MAX(LINE),0)+10 AS DefaultValue FROM NO_ROL_PAGO_PROVISION_LINE WHERE NO_ROL_PAGO_PROVISION_ID=? ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, NO_Rol_Pago_Provision_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefE2830DC1366F4D0297CB8B1DED23CF0C_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT no_rol_pago_provision_line.NO_Rol_Pago_Provision_ID AS NAME" +
      "        FROM no_rol_pago_provision_line" +
      "        WHERE no_rol_pago_provision_line.NO_Rol_Pago_Provision_Line_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String noRolPagoProvisionId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Documentno), ''))) AS NAME FROM no_rol_pago_provision left join (select NO_Rol_Pago_Provision_ID, Documentno from NO_Rol_Pago_Provision) table1 on (no_rol_pago_provision.NO_Rol_Pago_Provision_ID = table1.NO_Rol_Pago_Provision_ID) WHERE no_rol_pago_provision.NO_Rol_Pago_Provision_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String noRolPagoProvisionId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Documentno), ''))) AS NAME FROM no_rol_pago_provision left join (select NO_Rol_Pago_Provision_ID, Documentno from NO_Rol_Pago_Provision) table1 on (no_rol_pago_provision.NO_Rol_Pago_Provision_ID = table1.NO_Rol_Pago_Provision_ID) WHERE no_rol_pago_provision.NO_Rol_Pago_Provision_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE no_rol_pago_provision_line" +
      "        SET Line = TO_NUMBER(?) , NO_Tipo_Ingreso_Egreso_ID = (?) , Fechainicio = TO_DATE(?) , Fechafin = TO_DATE(?) , Valor = TO_NUMBER(?) , C_Currency_ID = (?) , Isactive = (?) , Docstatus = (?) , C_Doctype_ID = (?) , Documentno = (?) , Docaccionno = (?) , NO_Rol_Pago_Provision_Line_ID = (?) , AD_Org_ID = (?) , AD_Client_ID = (?) , NO_Rol_Pago_Provision_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE no_rol_pago_provision_line.NO_Rol_Pago_Provision_Line_ID = ? " +
      "                 AND no_rol_pago_provision_line.NO_Rol_Pago_Provision_ID = ? " +
      "        AND no_rol_pago_provision_line.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_rol_pago_provision_line.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, line);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noTipoIngresoEgresoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechainicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechafin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaccionno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionLineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionLineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO no_rol_pago_provision_line " +
      "        (Line, NO_Tipo_Ingreso_Egreso_ID, Fechainicio, Fechafin, Valor, C_Currency_ID, Isactive, Docstatus, C_Doctype_ID, Documentno, Docaccionno, NO_Rol_Pago_Provision_Line_ID, AD_Org_ID, AD_Client_ID, NO_Rol_Pago_Provision_ID, created, createdby, updated, updatedBy)" +
      "        VALUES (TO_NUMBER(?), (?), TO_DATE(?), TO_DATE(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, line);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noTipoIngresoEgresoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechainicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechafin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaccionno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionLineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String noRolPagoProvisionId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM no_rol_pago_provision_line" +
      "        WHERE no_rol_pago_provision_line.NO_Rol_Pago_Provision_Line_ID = ? " +
      "                 AND no_rol_pago_provision_line.NO_Rol_Pago_Provision_ID = ? " +
      "        AND no_rol_pago_provision_line.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_rol_pago_provision_line.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM no_rol_pago_provision_line" +
      "         WHERE no_rol_pago_provision_line.NO_Rol_Pago_Provision_Line_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM no_rol_pago_provision_line" +
      "         WHERE no_rol_pago_provision_line.NO_Rol_Pago_Provision_Line_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
