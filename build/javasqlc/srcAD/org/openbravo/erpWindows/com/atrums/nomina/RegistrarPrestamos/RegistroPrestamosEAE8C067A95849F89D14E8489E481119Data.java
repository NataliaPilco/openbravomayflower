//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.RegistrarPrestamos;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class RegistroPrestamosEAE8C067A95849F89D14E8489E481119Data implements FieldProvider {
static Logger log4j = Logger.getLogger(RegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String cBpartnerId;
  public String noTipoIngresoEgresoId;
  public String noTipoIngresoEgresoIdr;
  public String valorPrestamo;
  public String mesesCuota;
  public String cPeriodId;
  public String cPeriodIdr;
  public String valorCuota;
  public String docactionPro;
  public String docactionProBtn;
  public String adClientId;
  public String processed;
  public String isactive;
  public String sueldo;
  public String descuentoIess;
  public String totalRecibir;
  public String noPrestamoId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("no_tipo_ingreso_egreso_id") || fieldName.equals("noTipoIngresoEgresoId"))
      return noTipoIngresoEgresoId;
    else if (fieldName.equalsIgnoreCase("no_tipo_ingreso_egreso_idr") || fieldName.equals("noTipoIngresoEgresoIdr"))
      return noTipoIngresoEgresoIdr;
    else if (fieldName.equalsIgnoreCase("valor_prestamo") || fieldName.equals("valorPrestamo"))
      return valorPrestamo;
    else if (fieldName.equalsIgnoreCase("meses_cuota") || fieldName.equals("mesesCuota"))
      return mesesCuota;
    else if (fieldName.equalsIgnoreCase("c_period_id") || fieldName.equals("cPeriodId"))
      return cPeriodId;
    else if (fieldName.equalsIgnoreCase("c_period_idr") || fieldName.equals("cPeriodIdr"))
      return cPeriodIdr;
    else if (fieldName.equalsIgnoreCase("valor_cuota") || fieldName.equals("valorCuota"))
      return valorCuota;
    else if (fieldName.equalsIgnoreCase("docaction_pro") || fieldName.equals("docactionPro"))
      return docactionPro;
    else if (fieldName.equalsIgnoreCase("docaction_pro_btn") || fieldName.equals("docactionProBtn"))
      return docactionProBtn;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("sueldo"))
      return sueldo;
    else if (fieldName.equalsIgnoreCase("descuento_iess") || fieldName.equals("descuentoIess"))
      return descuentoIess;
    else if (fieldName.equalsIgnoreCase("total_recibir") || fieldName.equals("totalRecibir"))
      return totalRecibir;
    else if (fieldName.equalsIgnoreCase("no_prestamo_id") || fieldName.equals("noPrestamoId"))
      return noPrestamoId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static RegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static RegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(no_prestamo.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_prestamo.CreatedBy) as CreatedByR, " +
      "        to_char(no_prestamo.Updated, ?) as updated, " +
      "        to_char(no_prestamo.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        no_prestamo.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_prestamo.UpdatedBy) as UpdatedByR," +
      "        no_prestamo.AD_Org_ID, " +
      "(CASE WHEN no_prestamo.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "no_prestamo.C_Bpartner_ID, " +
      "no_prestamo.NO_Tipo_Ingreso_Egreso_ID, " +
      "(CASE WHEN no_prestamo.NO_Tipo_Ingreso_Egreso_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS NO_Tipo_Ingreso_Egreso_IDR, " +
      "no_prestamo.Valor_Prestamo, " +
      "no_prestamo.Meses_Cuota, " +
      "no_prestamo.C_Period_ID, " +
      "(CASE WHEN no_prestamo.C_Period_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Name), ''))),'') ) END) AS C_Period_IDR, " +
      "no_prestamo.Valor_Cuota, " +
      "no_prestamo.Docaction_Pro, " +
      "list1.name as Docaction_Pro_BTN, " +
      "no_prestamo.AD_Client_ID, " +
      "COALESCE(no_prestamo.Processed, 'N') AS Processed, " +
      "COALESCE(no_prestamo.Isactive, 'N') AS Isactive, " +
      "no_prestamo.Sueldo, " +
      "no_prestamo.Descuento_Iess, " +
      "no_prestamo.Total_Recibir, " +
      "no_prestamo.NO_Prestamo_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM no_prestamo left join (select AD_Org_ID, Name from AD_Org) table1 on (no_prestamo.AD_Org_ID = table1.AD_Org_ID) left join (select NO_Tipo_Ingreso_Egreso_ID, Name from NO_Tipo_Ingreso_Egreso) table2 on (no_prestamo.NO_Tipo_Ingreso_Egreso_ID = table2.NO_Tipo_Ingreso_Egreso_ID) left join (select C_Period_ID, Name from C_Period) table3 on (no_prestamo.C_Period_ID = table3.C_Period_ID) left join ad_ref_list_v list1 on (list1.ad_reference_id = '135' and list1.ad_language = ?  AND no_prestamo.Docaction_Pro = TO_CHAR(list1.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND no_prestamo.NO_Prestamo_ID = ? " +
      "        AND no_prestamo.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND no_prestamo.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        RegistroPrestamosEAE8C067A95849F89D14E8489E481119Data objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data = new RegistroPrestamosEAE8C067A95849F89D14E8489E481119Data();
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.created = UtilSql.getValue(result, "created");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.updated = UtilSql.getValue(result, "updated");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.noTipoIngresoEgresoId = UtilSql.getValue(result, "no_tipo_ingreso_egreso_id");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.noTipoIngresoEgresoIdr = UtilSql.getValue(result, "no_tipo_ingreso_egreso_idr");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.valorPrestamo = UtilSql.getValue(result, "valor_prestamo");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.mesesCuota = UtilSql.getValue(result, "meses_cuota");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.cPeriodId = UtilSql.getValue(result, "c_period_id");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.cPeriodIdr = UtilSql.getValue(result, "c_period_idr");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.valorCuota = UtilSql.getValue(result, "valor_cuota");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.docactionPro = UtilSql.getValue(result, "docaction_pro");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.docactionProBtn = UtilSql.getValue(result, "docaction_pro_btn");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.processed = UtilSql.getValue(result, "processed");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.isactive = UtilSql.getValue(result, "isactive");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.sueldo = UtilSql.getValue(result, "sueldo");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.descuentoIess = UtilSql.getValue(result, "descuento_iess");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.totalRecibir = UtilSql.getValue(result, "total_recibir");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.noPrestamoId = UtilSql.getValue(result, "no_prestamo_id");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.language = UtilSql.getValue(result, "language");
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.adUserClient = "";
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.adOrgClient = "";
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.createdby = "";
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.trBgcolor = "";
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.totalCount = "";
        objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    RegistroPrestamosEAE8C067A95849F89D14E8489E481119Data objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[] = new RegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[vector.size()];
    vector.copyInto(objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data);
    return(objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data);
  }

/**
Create a registry
 */
  public static RegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[] set(String docactionPro, String docactionProBtn, String noPrestamoId, String valorCuota, String descuentoIess, String processed, String cBpartnerId, String totalRecibir, String valorPrestamo, String mesesCuota, String noTipoIngresoEgresoId, String adOrgId, String isactive, String createdby, String createdbyr, String sueldo, String updatedby, String updatedbyr, String cPeriodId, String adClientId)    throws ServletException {
    RegistroPrestamosEAE8C067A95849F89D14E8489E481119Data objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[] = new RegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[1];
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0] = new RegistroPrestamosEAE8C067A95849F89D14E8489E481119Data();
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].created = "";
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].createdbyr = createdbyr;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].updated = "";
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].updatedTimeStamp = "";
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].updatedby = updatedby;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].updatedbyr = updatedbyr;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].adOrgId = adOrgId;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].adOrgIdr = "";
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].cBpartnerId = cBpartnerId;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].noTipoIngresoEgresoId = noTipoIngresoEgresoId;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].noTipoIngresoEgresoIdr = "";
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].valorPrestamo = valorPrestamo;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].mesesCuota = mesesCuota;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].cPeriodId = cPeriodId;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].cPeriodIdr = "";
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].valorCuota = valorCuota;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].docactionPro = docactionPro;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].docactionProBtn = docactionProBtn;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].adClientId = adClientId;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].processed = processed;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].isactive = isactive;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].sueldo = sueldo;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].descuentoIess = descuentoIess;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].totalRecibir = totalRecibir;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].noPrestamoId = noPrestamoId;
    objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data[0].language = "";
    return objectRegistroPrestamosEAE8C067A95849F89D14E8489E481119Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDefADF209E56E6645F8812DDCA6EDB63F5D_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefDBF99FB00B87453FBCA3FCB6B49E2D69_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefDFF59885E3104868BE3E88797C26B8A7(ConnectionProvider connectionProvider, String AD_ORG_ID, String AD_CLIENT_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT P.C_PERIOD_ID AS DEFAULTVALUE FROM C_PERIOD P WHERE EXISTS (SELECT * FROM C_PERIODCONTROL PC WHERE P.C_PERIOD_ID=PC.C_PERIOD_ID AND UPPER(PC.PERIODSTATUS)='O') AND EXISTS(SELECT * FROM C_CALENDAR C, C_YEAR Y WHERE Y.C_CALENDAR_ID=C.C_CALENDAR_ID AND P.C_YEAR_ID=Y.C_YEAR_ID AND AD_ISORGINCLUDED(?, C.AD_ORG_ID, ?)<> -1) AND P.AD_CLIENT_ID=? AND NOW() BETWEEN STARTDATE AND ENDDATE ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_ORG_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE no_prestamo" +
      "        SET AD_Org_ID = (?) , C_Bpartner_ID = (?) , NO_Tipo_Ingreso_Egreso_ID = (?) , Valor_Prestamo = TO_NUMBER(?) , Meses_Cuota = TO_NUMBER(?) , C_Period_ID = (?) , Valor_Cuota = TO_NUMBER(?) , Docaction_Pro = (?) , AD_Client_ID = (?) , Processed = (?) , Isactive = (?) , Sueldo = TO_NUMBER(?) , Descuento_Iess = TO_NUMBER(?) , Total_Recibir = TO_NUMBER(?) , NO_Prestamo_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE no_prestamo.NO_Prestamo_ID = ? " +
      "        AND no_prestamo.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_prestamo.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noTipoIngresoEgresoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorPrestamo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mesesCuota);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPeriodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorCuota);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docactionPro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sueldo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descuentoIess);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalRecibir);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noPrestamoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noPrestamoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO no_prestamo " +
      "        (AD_Org_ID, C_Bpartner_ID, NO_Tipo_Ingreso_Egreso_ID, Valor_Prestamo, Meses_Cuota, C_Period_ID, Valor_Cuota, Docaction_Pro, AD_Client_ID, Processed, Isactive, Sueldo, Descuento_Iess, Total_Recibir, NO_Prestamo_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), TO_NUMBER(?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noTipoIngresoEgresoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorPrestamo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mesesCuota);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPeriodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorCuota);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docactionPro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sueldo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descuentoIess);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalRecibir);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noPrestamoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM no_prestamo" +
      "        WHERE no_prestamo.NO_Prestamo_ID = ? " +
      "        AND no_prestamo.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_prestamo.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM no_prestamo" +
      "         WHERE no_prestamo.NO_Prestamo_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM no_prestamo" +
      "         WHERE no_prestamo.NO_Prestamo_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
