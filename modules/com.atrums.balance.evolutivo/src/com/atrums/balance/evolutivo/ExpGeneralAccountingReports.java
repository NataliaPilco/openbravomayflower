/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2009 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.atrums.balance.evolutivo;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.erpCommon.businessUtility.AccountingSchemaMiscData;
import org.openbravo.erpCommon.businessUtility.Tree;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.DateTimeData;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.erpCommon.utility.WindowTreeData;
import org.openbravo.xmlEngine.XmlDocument;

public class ExpGeneralAccountingReports extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strDateFrom = vars.getGlobalVariable("inpDateFrom",
          "ExpGeneralAccountingReports|dateFrom", "");
      String strDateTo = vars.getGlobalVariable("inpDateTo", "ExpGeneralAccountingReports|dateTo",
          "");
      String strDateFromRef = vars.getGlobalVariable("inpDateFromRef",
          "ExpGeneralAccountingReports|dateFromRef", "");
      String strDateToRef = vars.getGlobalVariable("inpDateToRef",
          "ExpGeneralAccountingReports|dateToRef", "");
      String strAsDateTo = vars.getGlobalVariable("inpAsDateTo",
          "ExpGeneralAccountingReports|asDateTo", "");
      String strAsDateToRef = vars.getGlobalVariable("inpAsDateToRef",
          "ExpGeneralAccountingReports|asDateToRef", "");
      String strElementValue = vars.getGlobalVariable("inpcElementvalueId",
          "ExpGeneralAccountingReports|C_ElementValue_ID", "");
      String strConImporte = vars.getGlobalVariable("inpConImporte",
          "ExpGeneralAccountingReports|conImporte", "N");
      String strConCodigo = vars.getGlobalVariable("inpConCodigo",
          "ExpGeneralAccountingReports|conCodigo", "N");
      String strConMensual = vars.getGlobalVariable("inpConMensual",
          "ExpGeneralAccountingReports|conMensual", "N");
      String strLevel = vars.getGlobalVariable("inpLevel", "ExpGeneralAccountingReports|level", "");
      printPageDataSheet(response, vars, "", "", strDateFrom, strDateTo, strDateFromRef,
          strDateToRef, strAsDateTo, strAsDateToRef, strElementValue, strConImporte, "", strLevel,
          strConCodigo, "", strConMensual);
    } else if (vars.commandIn("FIND", "XLS")) {
      String strcAcctSchemaId = vars.getStringParameter("inpcAcctSchemaId", "");
      String strAgno = vars.getRequiredGlobalVariable("inpAgno",
          "ExpGeneralAccountingReports|agno");
      String strAgnoRef = vars.getRequiredGlobalVariable("inpAgnoRef",
          "ExpGeneralAccountingReports|agnoRef");
      String strDateFrom = vars.getRequestGlobalVariable("inpDateFrom",
          "ExpGeneralAccountingReports|dateFrom");
      String strDateTo = vars.getRequestGlobalVariable("inpDateTo",
          "ExpGeneralAccountingReports|dateTo");
      String strDateFromRef = vars.getRequestGlobalVariable("inpDateFromRef",
          "ExpGeneralAccountingReports|dateFromRef");
      String strDateToRef = vars.getRequestGlobalVariable("inpDateToRef",
          "ExpGeneralAccountingReports|dateToRef");
      String strAsDateTo = vars.getRequestGlobalVariable("inpAsDateTo",
          "ExpGeneralAccountingReports|asDateTo");
      String strAsDateToRef = vars.getRequestGlobalVariable("inpAsDateToRef",
          "ExpGeneralAccountingReports|asDateToRef");
      String strElementValue = vars.getRequiredGlobalVariable("inpcElementvalueId",
          "ExpGeneralAccountingReports|C_ElementValue_ID");
      String strConImporte = vars.getRequestGlobalVariable("inpConImporte",
          "ExpGeneralAccountingReports|conImporte");
      String strConCodigo = vars.getRequestGlobalVariable("inpConCodigo",
          "ExpGeneralAccountingReports|conCodigo");
      String strConMensual = vars.getRequestGlobalVariable("inpConMensual",
          "ExpGeneralAccountingReports|conMensual");
      String strOrg = vars.getRequestGlobalVariable("inpOrganizacion",
          "ExpGeneralAccountingReports|organizacion");
      String strLevel = vars.getRequestGlobalVariable("inpLevel",
          "ExpGeneralAccountingReports|level");
      printPagePDF(request, response, vars, strAgno, strAgnoRef, strDateFrom, strDateTo,
          strDateFromRef, strDateToRef, strAsDateTo, strAsDateToRef, strElementValue, strConImporte,
          strOrg, strLevel, strConCodigo, strcAcctSchemaId, strConMensual);
    } else
      pageError(response);
  }

  private void printPagePDF(HttpServletRequest request, HttpServletResponse response,
      VariablesSecureApp vars, String strAgno, String strAgnoRef, String strDateFrom,
      String strDateTo, String strDateFromRef, String strDateToRef, String strAsDateTo,
      String strAsDateToRef, String strElementValue, String strConImporte, String strOrg,
      String strLevel, String strConCodigo, String strcAcctSchemaId, String strConMensual)
      throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: pdf");
    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate("com/atrums/balance/evolutivo/ExpGeneralAccountingReportsPDF")
        .createXmlDocument();

    String strCalculateOpening = strElementValue.substring(0, 1);
    strElementValue = strElementValue.substring(1, strElementValue.length());
    ExpGeneralAccountingReportsData[] strGroups = ExpGeneralAccountingReportsData.selectGroups(this,
        strElementValue);

    try {
      strGroups[strGroups.length - 1].pagebreak = "";

      String[][] strElementValueDes = new String[strGroups.length][];
      if (log4j.isDebugEnabled())
        log4j.debug(
            "strElementValue:" + strElementValue + " - strGroups.length:" + strGroups.length);
      for (int i = 0; i < strGroups.length; i++) {
        ExpGeneralAccountingReportsData[] strElements = ExpGeneralAccountingReportsData
            .selectElements(this, strGroups[i].id);
        strElementValueDes[i] = new String[strElements.length];
        if (log4j.isDebugEnabled())
          log4j.debug("strElements.length:" + strElements.length);
        for (int j = 0; j < strElements.length; j++) {
          strElementValueDes[i][j] = strElements[j].id;
        }
      }

      String strTreeOrg = ExpGeneralAccountingReportsData.treeOrg(this, vars.getClient());
      AccountTree[] acct = new AccountTree[strGroups.length];

      AccountTreeData[][] elements = new AccountTreeData[strGroups.length][];

      WindowTreeData[] dataTree = WindowTreeData.selectTreeID(this,
          Utility.stringList(vars.getClient()), "EV");
      String TreeID = "";
      if (dataTree != null && dataTree.length != 0)
        TreeID = dataTree[0].id;

      // For each year, the initial and closing date is obtained
      ExpGeneralAccountingReportsData[] startEndYear = ExpGeneralAccountingReportsData
          .startEndYear(this, vars.getClient(), "'" + strAgno + "'");
      ExpGeneralAccountingReportsData[] startEndYearRef = ExpGeneralAccountingReportsData
          .startEndYear(this, vars.getClient(), "'" + strAgnoRef + "'");
      ExpGeneralAccountingReportsData[] refMes = ExpGeneralAccountingReportsData.selectMes(this,
          startEndYearRef[0].id);
      String strYear = "'" + startEndYear[0].name + "'";
      String strYearRef = "'" + startEndYearRef[0].name + "'";
      String strYearsToClose = "";
      String strYearsToCloseRef = "";
      String strAnioRef = startEndYearRef[0].name;

      Vector<Object> vec = new Vector<Object>();
      // Relation of open and closed years is obtained
      ExpGeneralAccountingReportsData[] closedYears = ExpGeneralAccountingReportsData
          .checkFiscalYears(this, vars.getClient());
      ExpGeneralAccountingReportsData[] previousYears = ExpGeneralAccountingReportsData
          .previousYear(this, vars.getClient());
      if (strCalculateOpening.equals("Y")) {
        strCalculateOpening = "N";
        strDateTo = strAsDateTo;
        strDateToRef = strAsDateToRef;
        strDateFrom = "";
        strDateFromRef = "";
        fulfillYearsToClose(vec, startEndYear[0].name, closedYears, previousYears);
        // If there is some year to close, will be appended to the year to show in report when
        // calculating amounts, through strYearsToClose and strYearsToCloseRef variables
        if (vec.size() > 0) {
          for (int i = 0; i < vec.size(); i++) {
            strCalculateOpening = "Y"; // If we finally calculate the opening variable takes "Y"
            strYearsToClose = strYearsToClose + ",'" + vec.elementAt(i) + "'";
          }
        }
        vec = new Vector<Object>();
        fulfillYearsToClose(vec, startEndYearRef[0].name, closedYears, previousYears);
        // If there is some year to close
        if (vec.size() > 0) {
          for (int i = 0; i < vec.size(); i++) {
            strCalculateOpening = "Y"; // If we finally calculate the opening variable takes "Y"
            strYearsToCloseRef = strYearsToCloseRef + ",'" + vec.elementAt(i) + "'";
          }
        }
      }

      for (int i = 0; i < strGroups.length; i++) {
        // All account tree is obtained
        if (vars.getLanguage().equals("en_US")) {
          elements[i] = AccountTreeData.select(this, strConCodigo, TreeID);
        } else {
          elements[i] = AccountTreeData.selectTrl(this, strConCodigo, vars.getLanguage(), TreeID);
        }
        // For each account with movements in the year, debit and credit total amounts are
        // calculated according to fact_acct movements.

        // AccountTreeData[] accounts = null;

        AccountTreeData[] accounts = null;

        if (strConMensual.equals("Y")) {
          accounts = AccountTreeData.selectAcctPresupuesto(this,
              Utility.getContext(this, vars, "#AccessibleOrgTree", "ExpGeneralAccountingReports"),
              Utility.getContext(this, vars, "#User_Client", "ExpGeneralAccountingReports"),
              strDateFrom, DateTimeData.nDaysAfter(this, strDateTo, "1"), strcAcctSchemaId,
              Tree.getMembers(this, strTreeOrg, strOrg), strYear + strYearsToClose, strDateFromRef,
              DateTimeData.nDaysAfter(this, strDateToRef, "1"), strYearRef + strYearsToCloseRef,
              strOrg, vars.getClient(), strcAcctSchemaId, strAnioRef, strOrg, vars.getClient(),
              strcAcctSchemaId, strAnioRef, strOrg, vars.getClient(), strcAcctSchemaId, strAnioRef,
              strOrg, vars.getClient(), strcAcctSchemaId, strAnioRef, strOrg, vars.getClient(),
              strcAcctSchemaId, strAnioRef, strOrg, vars.getClient(), strcAcctSchemaId, strAnioRef,
              strOrg, vars.getClient(), strcAcctSchemaId, strAnioRef, strOrg, vars.getClient(),
              strcAcctSchemaId, strAnioRef, strOrg, vars.getClient(), strcAcctSchemaId, strAnioRef,
              strOrg, vars.getClient(), strcAcctSchemaId, strAnioRef, strOrg, vars.getClient(),
              strcAcctSchemaId, strAnioRef, strOrg, vars.getClient(), strcAcctSchemaId, strAnioRef,
              strOrg, vars.getClient(), strAnioRef, strOrg, vars.getClient(), strAnioRef, strOrg,
              vars.getClient(), strAnioRef, strOrg, vars.getClient(), strAnioRef, strOrg,
              vars.getClient(), strAnioRef, strOrg, vars.getClient(), strAnioRef, strOrg,
              vars.getClient(), strAnioRef, strOrg, vars.getClient(), strAnioRef, strOrg,
              vars.getClient(), strAnioRef, strOrg, vars.getClient(), strAnioRef, strOrg,
              vars.getClient(), strAnioRef, strOrg, vars.getClient(), strAnioRef);

        } else {
          accounts = AccountTreeData.selectAcct(this,
              Utility.getContext(this, vars, "#AccessibleOrgTree", "ExpGeneralAccountingReports"),
              Utility.getContext(this, vars, "#User_Client", "ExpGeneralAccountingReports"),
              strDateFrom, DateTimeData.nDaysAfter(this, strDateTo, "1"), strcAcctSchemaId,
              Tree.getMembers(this, strTreeOrg, strOrg), strYear + strYearsToClose, strDateFromRef,
              DateTimeData.nDaysAfter(this, strDateToRef, "1"), strYearRef + strYearsToCloseRef,
              strOrg, vars.getClient(), strcAcctSchemaId, strAnioRef, strOrg, vars.getClient(),
              strcAcctSchemaId, strAnioRef, strOrg, vars.getClient(), strcAcctSchemaId, strAnioRef,
              strOrg, vars.getClient(), strcAcctSchemaId, strAnioRef, strOrg, vars.getClient(),
              strcAcctSchemaId, strAnioRef, strOrg, vars.getClient(), strcAcctSchemaId, strAnioRef,
              strOrg, vars.getClient(), strcAcctSchemaId, strAnioRef, strOrg, vars.getClient(),
              strcAcctSchemaId, strAnioRef, strOrg, vars.getClient(), strcAcctSchemaId, strAnioRef,
              strOrg, vars.getClient(), strcAcctSchemaId, strAnioRef, strOrg, vars.getClient(),
              strcAcctSchemaId, strAnioRef, strOrg, vars.getClient(), strcAcctSchemaId, strAnioRef);
        }

        {
          // Income summary amount is calculated and included in the balance sheet data
          String strIncomeSummary = ExpGeneralAccountingReportsData.incomesummary(this,
              strcAcctSchemaId);
          if (log4j.isDebugEnabled())
            log4j.debug("*********** strIncomeSummary: " + strIncomeSummary);
          String strISyear = processIncomeSummary(strDateFrom,
              DateTimeData.nDaysAfter(this, strDateTo, "1"), strYear + strYearsToClose, strTreeOrg,
              strOrg, strcAcctSchemaId);
          if (log4j.isDebugEnabled())
            log4j.debug("*********** strISyear: " + strISyear);
          String strISyearRef = processIncomeSummary(strDateFromRef,
              DateTimeData.nDaysAfter(this, strDateToRef, "1"), strYearRef + strYearsToCloseRef,
              strTreeOrg, strOrg, strcAcctSchemaId);
          if (log4j.isDebugEnabled())
            log4j.debug("*********** strISyearRef: " + strISyearRef);
          accounts = appendRecords(accounts, strIncomeSummary, strISyear, strISyearRef);

        }
        // Report tree is built with given the account tree, and the amounts obtained from fact_acct
        acct[i] = new AccountTree(vars, this, elements[i], accounts, strElementValueDes[i]);
        if (acct[i] != null) {
          acct[i].filterSVC();
          acct[i].filter(strConImporte.equals("Y"), strLevel, false);
        } else if (log4j.isDebugEnabled())
          log4j.debug("acct null!!!");
      }

      xmlDocument.setData("group", strGroups);

      xmlDocument.setParameter("agno", startEndYear[0].name);
      xmlDocument.setParameter("agno2", startEndYearRef[0].name);
      xmlDocument.setParameter("column", startEndYear[0].name);
      xmlDocument.setParameter("columnRef", startEndYearRef[0].name);
      xmlDocument.setParameter("org", OrganizationData.selectOrgName(this, strOrg));
      xmlDocument.setParameter("column1", startEndYear[0].name);
      xmlDocument.setParameter("columnRef1", startEndYearRef[0].name);
      /************************** NUEVOS CAMPOS ****************************/
      xmlDocument.setParameter("columnMes1", refMes[0].name);
      xmlDocument.setParameter("columnMes2", refMes[1].name);
      xmlDocument.setParameter("columnMes3", refMes[2].name);
      xmlDocument.setParameter("columnMes4", refMes[3].name);
      xmlDocument.setParameter("columnMes5", refMes[4].name);
      xmlDocument.setParameter("columnMes6", refMes[5].name);
      xmlDocument.setParameter("columnMes7", refMes[6].name);
      xmlDocument.setParameter("columnMes8", refMes[7].name);
      xmlDocument.setParameter("columnMes9", refMes[8].name);
      xmlDocument.setParameter("columnMes10", refMes[9].name);
      xmlDocument.setParameter("columnMes11", refMes[10].name);
      xmlDocument.setParameter("columnMes12", refMes[11].name);
      /*********************************************************************/
      xmlDocument.setParameter("companyName",
          ExpGeneralAccountingReportsData.companyName(this, vars.getClient()));
      xmlDocument.setParameter("date", DateTimeData.today(this));
      if (strDateFrom.equals(""))
        strDateFrom = startEndYear[0].begining;
      if (strDateTo.equals(""))
        strDateTo = startEndYear[0].end;
      if (strDateFromRef.equals(""))
        strDateFromRef = startEndYearRef[0].begining;
      if (strDateToRef.equals(""))
        strDateToRef = startEndYearRef[0].end;
      xmlDocument.setParameter("period", strDateFrom + " - " + strDateTo);
      xmlDocument.setParameter("periodRef", strDateFromRef + " - " + strDateToRef);
      xmlDocument.setParameter("agnoInitial", startEndYear[0].name);
      xmlDocument.setParameter("agnoRef", startEndYearRef[0].name);

      xmlDocument.setParameter("principalTitle",
          strCalculateOpening.equals("Y")
              ? ExpGeneralAccountingReportsData.rptTitle(this, strElementValue) + " (Provisional)"
              : ExpGeneralAccountingReportsData.rptTitle(this, strElementValue));

      AccountTreeData[][] trees = new AccountTreeData[strGroups.length][];

      for (int i = 0; i < strGroups.length; i++)
        trees[i] = acct[i].getAccounts();

      if (vars.commandIn("XLS")) {

        if (strGroups.length > 1) {
          AccountTreeData[] array = new AccountTreeData[trees[0].length + trees[1].length];

          for (int i = 0; i < trees[0].length; i++) {
            array[i] = trees[0][i];
          }
          for (int i = 0; i < trees[1].length; i++) {
            array[(trees[0].length) + i] = trees[1][i];
          }

          trees[0] = array;

        }
        // if (strGroups.length > 1) {
        // AccountTreeData[] result = Arrays.copyOf(trees[0], trees[0].length + trees[1].length);
        // System.arraycopy(trees[1], 0, result, trees[0].length, trees[1].length);
        // trees[0] = result;
        // }

        String strReportName = "";

        if (strConMensual.equals("Y")) {
          strReportName = "@basedesign@/com/atrums/balance/evolutivo/GeneralAccountingReportsPresupuesto.jrxml";
        } else {
          strReportName = "@basedesign@/com/atrums/balance/evolutivo/GeneralAccountingReports.jrxml";
        }
        response.setHeader("Content-disposition", "inline; filename=ReportGeneralLedgerPDF.pdf");
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("date", DateTimeData.today(this));
        parameters.put("org", OrganizationData.selectOrgName(this, strOrg));
        parameters.put("companyName",
            ExpGeneralAccountingReportsData.companyName(this, vars.getClient()));
        parameters.put("column1", startEndYear[0].name);
        parameters.put("columnRef1", startEndYearRef[0].name);

        /********************* NUEVOS CAMPOS *************************/
        parameters.put("columnMes1", refMes[0].name);
        parameters.put("columnMes2", refMes[1].name);
        parameters.put("columnMes3", refMes[2].name);
        parameters.put("columnMes4", refMes[3].name);
        parameters.put("columnMes5", refMes[4].name);
        parameters.put("columnMes6", refMes[5].name);
        parameters.put("columnMes7", refMes[6].name);
        parameters.put("columnMes8", refMes[7].name);
        parameters.put("columnMes9", refMes[8].name);
        parameters.put("columnMes10", refMes[9].name);
        parameters.put("columnMes11", refMes[10].name);
        parameters.put("columnMes12", refMes[11].name);
        /*************************************************************/
        parameters.put("period", strDateFrom + " - " + strDateTo);
        parameters.put("periodRef", strDateFromRef + " - " + strDateToRef);
        parameters.put("report", ExpGeneralAccountingReportsData.rptTitle(this, strElementValue));
        renderJR(vars, response, strReportName, "xls", parameters, trees[0], null);

      } else {

        xmlDocument.setDataArray("reportDetail", "structure1", trees);

        String strResult = xmlDocument.print();
        renderFO(strResult, response);

      }

    } catch (ArrayIndexOutOfBoundsException e) {
      advisePopUp(request, response, "ERROR",
          Utility.messageBD(this, "ReportWithoutNodes", vars.getLanguage()));

    }
  }

  private Vector<Object> fulfillYearsToClose(Vector<Object> vec, String yearID,
      ExpGeneralAccountingReportsData[] closedYears,
      ExpGeneralAccountingReportsData[] previousYears) {
    Vector<Object> vecAux = vec;
    String previous = "";
    // Let's see if this year has a previous one
    for (int i = 0; i < previousYears.length; i++) {
      if (previousYears[i].name.equals(yearID))
        previous = previousYears[i].previousYear;
    }
    // If not, return with what we have got until now
    if ("".equals(previous))
      return vecAux;

    // Let's see if the previous year was closed or not
    for (int i = 0; i < closedYears.length; i++) {
      if (closedYears[i].name.substring(0, 4).equals(yearID))
        if ("N".equals(closedYears[i].id.substring(0, 1))) {
          if (!vecAux.contains(previous))
            vecAux.add(previous);
          // If not, let's check if previous of the previous was closed
          vecAux = fulfillYearsToClose(vecAux, previous, closedYears, previousYears);
        }
    }
    return vecAux;
  }

  private AccountTreeData[] appendRecords(AccountTreeData[] data, String strIncomeSummary,
      String strISyear, String strISyearRef) throws ServletException {
    if (data == null || strIncomeSummary == null || strIncomeSummary.equals("") || strISyear == null
        || strISyear.equals("") || strISyearRef == null || strISyearRef.equals(""))
      return data;
    AccountTreeData[] data2 = new AccountTreeData[data.length + 1];
    boolean found = false;
    for (int i = 0; i < data.length; i++) {
      if (data[i].id.equals(strIncomeSummary)) {
        found = true;
        BigDecimal isYear = new BigDecimal(strISyear);
        BigDecimal isYearRef = new BigDecimal(strISyearRef);

        data[i].qty = (new BigDecimal(data[i].qty).add(isYear)).toPlainString();
        data[i].qtycredit = (new BigDecimal(data[i].qtycredit).add(isYear)).toPlainString();
        data[i].qtyRef = (new BigDecimal(data[i].qtyRef).add(isYearRef)).toPlainString();
        data[i].qtycreditRef = (new BigDecimal(data[i].qtycreditRef).add(isYearRef))
            .toPlainString();
        data[i].qtyMes1 = (new BigDecimal(data[i].qtyMes1).add(isYearRef)).toPlainString(); // REP
        data[i].qtyMes2 = (new BigDecimal(data[i].qtyMes2).add(isYearRef)).toPlainString(); // REP
        data[i].qtyMes3 = (new BigDecimal(data[i].qtyMes3).add(isYearRef)).toPlainString(); // REP
        data[i].qtyMes4 = (new BigDecimal(data[i].qtyMes4).add(isYearRef)).toPlainString(); // REP
        data[i].qtyMes5 = (new BigDecimal(data[i].qtyMes5).add(isYearRef)).toPlainString(); // REP
        data[i].qtyMes6 = (new BigDecimal(data[i].qtyMes6).add(isYearRef)).toPlainString(); // REP
        data[i].qtyMes7 = (new BigDecimal(data[i].qtyMes7).add(isYearRef)).toPlainString(); // REP
        data[i].qtyMes8 = (new BigDecimal(data[i].qtyMes8).add(isYearRef)).toPlainString(); // REP
        data[i].qtyMes9 = (new BigDecimal(data[i].qtyMes9).add(isYearRef)).toPlainString(); // REP
        data[i].qtyMes10 = (new BigDecimal(data[i].qtyMes10).add(isYearRef)).toPlainString(); // REP
        data[i].qtyMes11 = (new BigDecimal(data[i].qtyMes11).add(isYearRef)).toPlainString(); // REP
        data[i].qtyMes12 = (new BigDecimal(data[i].qtyMes12).add(isYearRef)).toPlainString(); // REP

        // Añadido Presupuesto
        data[i].qtyMes1Presupuesto = (new BigDecimal(data[i].qtyMes1Presupuesto).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes2Presupuesto = (new BigDecimal(data[i].qtyMes2Presupuesto).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes3Presupuesto = (new BigDecimal(data[i].qtyMes3Presupuesto).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes4Presupuesto = (new BigDecimal(data[i].qtyMes4Presupuesto).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes5Presupuesto = (new BigDecimal(data[i].qtyMes5Presupuesto).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes6Presupuesto = (new BigDecimal(data[i].qtyMes6Presupuesto).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes7Presupuesto = (new BigDecimal(data[i].qtyMes7Presupuesto).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes8Presupuesto = (new BigDecimal(data[i].qtyMes8Presupuesto).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes9Presupuesto = (new BigDecimal(data[i].qtyMes9Presupuesto).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes10Presupuesto = (new BigDecimal(data[i].qtyMes10Presupuesto).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes11Presupuesto = (new BigDecimal(data[i].qtyMes11Presupuesto).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes12Presupuesto = (new BigDecimal(data[i].qtyMes12Presupuesto).add(isYearRef))
            .toPlainString(); // REP

        // Añadido Diferencia
        data[i].qtyMes1Diferencia = (new BigDecimal(data[i].qtyMes1Diferencia).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes2Diferencia = (new BigDecimal(data[i].qtyMes2Diferencia).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes3Diferencia = (new BigDecimal(data[i].qtyMes3Diferencia).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes4Diferencia = (new BigDecimal(data[i].qtyMes4Diferencia).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes5Diferencia = (new BigDecimal(data[i].qtyMes5Diferencia).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes6Diferencia = (new BigDecimal(data[i].qtyMes6Diferencia).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes7Diferencia = (new BigDecimal(data[i].qtyMes7Diferencia).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes8Diferencia = (new BigDecimal(data[i].qtyMes8Diferencia).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes9Diferencia = (new BigDecimal(data[i].qtyMes9Diferencia).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes10Diferencia = (new BigDecimal(data[i].qtyMes10Diferencia).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes11Diferencia = (new BigDecimal(data[i].qtyMes11Diferencia).add(isYearRef))
            .toPlainString(); // REP
        data[i].qtyMes12Diferencia = (new BigDecimal(data[i].qtyMes12Diferencia).add(isYearRef))
            .toPlainString(); // REP
      }
      data2[i] = data[i];
    }
    if (!found) {
      data2[data2.length - 1] = new AccountTreeData();
      data2[data2.length - 1].id = strIncomeSummary;
      data2[data2.length - 1].qty = strISyear;
      data2[data2.length - 1].qtycredit = strISyear;
      data2[data2.length - 1].qtyRef = strISyearRef;
      data2[data2.length - 1].qtycreditRef = strISyearRef;
      data2[data2.length - 1].qtyMes1 = strISyearRef; // REP
      data2[data2.length - 1].qtyMes2 = strISyearRef; // REP
      data2[data2.length - 1].qtyMes3 = strISyearRef; // REP
      data2[data2.length - 1].qtyMes4 = strISyearRef; // REP
      data2[data2.length - 1].qtyMes5 = strISyearRef; // REP
      data2[data2.length - 1].qtyMes6 = strISyearRef; // REP
      data2[data2.length - 1].qtyMes7 = strISyearRef; // REP
      data2[data2.length - 1].qtyMes8 = strISyearRef; // REP
      data2[data2.length - 1].qtyMes9 = strISyearRef; // REP
      data2[data2.length - 1].qtyMes10 = strISyearRef; // REP
      data2[data2.length - 1].qtyMes11 = strISyearRef; // REP
      data2[data2.length - 1].qtyMes12 = strISyearRef; // REP

      // Añadido Presupuesto
      data2[data2.length - 1].qtyMes1Presupuesto = strISyearRef; // REP
      data2[data2.length - 1].qtyMes2Presupuesto = strISyearRef; // REP
      data2[data2.length - 1].qtyMes3Presupuesto = strISyearRef; // REP
      data2[data2.length - 1].qtyMes4Presupuesto = strISyearRef; // REP
      data2[data2.length - 1].qtyMes5Presupuesto = strISyearRef; // REP
      data2[data2.length - 1].qtyMes6Presupuesto = strISyearRef; // REP
      data2[data2.length - 1].qtyMes7Presupuesto = strISyearRef; // REP
      data2[data2.length - 1].qtyMes8Presupuesto = strISyearRef; // REP
      data2[data2.length - 1].qtyMes9Presupuesto = strISyearRef; // REP
      data2[data2.length - 1].qtyMes10Presupuesto = strISyearRef; // REP
      data2[data2.length - 1].qtyMes11Presupuesto = strISyearRef; // REP
      data2[data2.length - 1].qtyMes12Presupuesto = strISyearRef; // REP

      // Añadido Diferencia
      data2[data2.length - 1].qtyMes1Diferencia = strISyearRef; // REP
      data2[data2.length - 1].qtyMes2Diferencia = strISyearRef; // REP
      data2[data2.length - 1].qtyMes3Diferencia = strISyearRef; // REP
      data2[data2.length - 1].qtyMes4Diferencia = strISyearRef; // REP
      data2[data2.length - 1].qtyMes5Diferencia = strISyearRef; // REP
      data2[data2.length - 1].qtyMes6Diferencia = strISyearRef; // REP
      data2[data2.length - 1].qtyMes7Diferencia = strISyearRef; // REP
      data2[data2.length - 1].qtyMes8Diferencia = strISyearRef; // REP
      data2[data2.length - 1].qtyMes9Diferencia = strISyearRef; // REP
      data2[data2.length - 1].qtyMes10Diferencia = strISyearRef; // REP
      data2[data2.length - 1].qtyMes11Diferencia = strISyearRef; // REP
      data2[data2.length - 1].qtyMes12Diferencia = strISyearRef; // REP

    } else
      return data;
    return data2;
  }

  private String processIncomeSummary(String strDateFrom, String strDateTo, String strAgno,
      String strTreeOrg, String strOrg, String strcAcctSchemaId)
      throws ServletException, IOException {
    String strISRevenue = ExpGeneralAccountingReportsData.selectPyG(this, "R", strDateFrom,
        strDateTo, strcAcctSchemaId, strAgno, Tree.getMembers(this, strTreeOrg, strOrg));
    String strISExpense = ExpGeneralAccountingReportsData.selectPyG(this, "E", strDateFrom,
        strDateTo, strcAcctSchemaId, strAgno, Tree.getMembers(this, strTreeOrg, strOrg));
    BigDecimal totalRevenue = new BigDecimal(strISRevenue);
    BigDecimal totalExpense = new BigDecimal(strISExpense);
    BigDecimal total = totalRevenue.add(totalExpense);
    if (log4j.isDebugEnabled())
      log4j.debug(total.toString());
    return total.toString();
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
      String strAgno, String strAgnoRef, String strDateFrom, String strDateTo,
      String strDateFromRef, String strDateToRef, String strAsDateTo, String strAsDateToRef,
      String strElementValue, String strConImporte, String strOrg, String strLevel,
      String strConCodigo, String strcAcctSchemaId, String strConMensual)
      throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate("com/atrums/balance/evolutivo/ExpGeneralAccountingReports")
        .createXmlDocument();

    ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "ExpGeneralAccountingReports", false,
        "", "", "", false, "ad_reports", strReplaceWith, false, true);
    toolbar.prepareSimpleToolBarTemplate();
    xmlDocument.setParameter("toolbar", toolbar.toString());

    try {
      WindowTabs tabs = new WindowTabs(this, vars,
          "com.atrums.balance.evolutivo.ExpGeneralAccountingReports");
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      xmlDocument.setParameter("theme", vars.getTheme());
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(),
          "ExpGeneralAccountingReports.html", classInfo.id, classInfo.type, strReplaceWith,
          tabs.breadcrumb());
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(),
          "ExpGeneralAccountingReports.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage("ExpGeneralAccountingReports");
      vars.removeMessage("ExpGeneralAccountingReports");
      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }

    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("agno", strAgno);
    xmlDocument.setParameter("agnoRef", strAgnoRef);
    xmlDocument.setParameter("dateFrom", strDateFrom);
    xmlDocument.setParameter("dateFromdisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateFromsaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateTo", strDateTo);
    xmlDocument.setParameter("dateTodisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateTosaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateFromRef", strDateFromRef);
    xmlDocument.setParameter("dateFromRefdisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateFromRefsaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateToRef", strDateToRef);
    xmlDocument.setParameter("dateToRefdisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateToRefsaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("asDateTo", strAsDateTo);
    xmlDocument.setParameter("asDateTodisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("asDateTosaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("asDateToRef", strAsDateToRef);
    xmlDocument.setParameter("asDateToRefdisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("asDateToRefsaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("conImporte", strConImporte);
    xmlDocument.setParameter("conCodigo", strConCodigo);
    xmlDocument.setParameter("conMensual", strConMensual);
    xmlDocument.setParameter("C_Org_ID", strOrg);
    xmlDocument.setParameter("C_ElementValue_ID", strElementValue);
    xmlDocument.setParameter("level", strLevel);
    xmlDocument.setParameter("cAcctschemaId", strcAcctSchemaId);
    xmlDocument.setData("reportC_ACCTSCHEMA_ID", "liststructure",
        AccountingSchemaMiscData.selectC_ACCTSCHEMA_ID(this,
            Utility.getContext(this, vars, "#AccessibleOrgTree", "ExpGeneralAccountingReports"),
            Utility.getContext(this, vars, "#User_Client", "ExpGeneralAccountingReports"),
            strcAcctSchemaId));
    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "LIST", "",
          "C_ElementValue level", "",
          Utility.getContext(this, vars, "#AccessibleOrgTree", "ExpGeneralAccountingReports"),
          Utility.getContext(this, vars, "#User_Client", "ExpGeneralAccountingReports"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "ExpGeneralAccountingReports",
          "");
      xmlDocument.setData("reportLevel", "liststructure", comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    xmlDocument.setParameter("orgs", Utility.arrayDobleEntrada("arrOrgs",
        ExpGeneralAccountingReportsData.selectOrgsDouble(this, vars.getClient())));
    xmlDocument.setParameter("accountingReports", Utility.arrayDobleEntrada("arrAccountingReports",
        ExpGeneralAccountingReportsData.selectRptDouble(this)));
    xmlDocument.setParameter("years", Utility.arrayDobleEntrada("arrYears",
        ExpGeneralAccountingReportsData.selectYearsDouble(this, vars.getUserClient())));
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  public String getServletInfo() {
    return "Servlet ExpGeneralAccountingReportsData";
  } // end of getServletInfo() method
}
