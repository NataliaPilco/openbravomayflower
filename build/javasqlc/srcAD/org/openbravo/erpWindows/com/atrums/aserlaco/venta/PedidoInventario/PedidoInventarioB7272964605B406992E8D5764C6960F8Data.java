//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.aserlaco.venta.PedidoInventario;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class PedidoInventarioB7272964605B406992E8D5764C6960F8Data implements FieldProvider {
static Logger log4j = Logger.getLogger(PedidoInventarioB7272964605B406992E8D5764C6960F8Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String mWarehouseId;
  public String cBpartnerId;
  public String fecha;
  public String isactive;
  public String docstatus;
  public String docstatusr;
  public String procesar;
  public String procesarBtn;
  public String asvPedidoInventarioId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("m_warehouse_id") || fieldName.equals("mWarehouseId"))
      return mWarehouseId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("fecha"))
      return fecha;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("docstatusr"))
      return docstatusr;
    else if (fieldName.equalsIgnoreCase("procesar"))
      return procesar;
    else if (fieldName.equalsIgnoreCase("procesar_btn") || fieldName.equals("procesarBtn"))
      return procesarBtn;
    else if (fieldName.equalsIgnoreCase("asv_pedido_inventario_id") || fieldName.equals("asvPedidoInventarioId"))
      return asvPedidoInventarioId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static PedidoInventarioB7272964605B406992E8D5764C6960F8Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static PedidoInventarioB7272964605B406992E8D5764C6960F8Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(asv_pedido_inventario.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = asv_pedido_inventario.CreatedBy) as CreatedByR, " +
      "        to_char(asv_pedido_inventario.Updated, ?) as updated, " +
      "        to_char(asv_pedido_inventario.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        asv_pedido_inventario.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = asv_pedido_inventario.UpdatedBy) as UpdatedByR," +
      "        asv_pedido_inventario.AD_Org_ID, " +
      "(CASE WHEN asv_pedido_inventario.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "asv_pedido_inventario.M_Warehouse_ID, " +
      "asv_pedido_inventario.C_Bpartner_ID, " +
      "asv_pedido_inventario.Fecha, " +
      "COALESCE(asv_pedido_inventario.Isactive, 'N') AS Isactive, " +
      "asv_pedido_inventario.Docstatus, " +
      "(CASE WHEN asv_pedido_inventario.Docstatus IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS DocstatusR, " +
      "asv_pedido_inventario.procesar, " +
      "list2.name as procesar_BTN, " +
      "asv_pedido_inventario.ASV_Pedido_Inventario_ID, " +
      "asv_pedido_inventario.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM asv_pedido_inventario left join (select AD_Org_ID, Name from AD_Org) table1 on (asv_pedido_inventario.AD_Org_ID = table1.AD_Org_ID) left join ad_ref_list_v list1 on (asv_pedido_inventario.Docstatus = list1.value and list1.ad_reference_id = 'B953095C5CB94B939B468DA0E887D8D2' and list1.ad_language = ?)  left join ad_ref_list_v list2 on (list2.ad_reference_id = '58F1F6B9031E42958986214558A889C2' and list2.ad_language = ?  AND asv_pedido_inventario.procesar = TO_CHAR(list2.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND asv_pedido_inventario.ASV_Pedido_Inventario_ID = ? " +
      "        AND asv_pedido_inventario.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND asv_pedido_inventario.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PedidoInventarioB7272964605B406992E8D5764C6960F8Data objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data = new PedidoInventarioB7272964605B406992E8D5764C6960F8Data();
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.created = UtilSql.getValue(result, "created");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.updated = UtilSql.getValue(result, "updated");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.mWarehouseId = UtilSql.getValue(result, "m_warehouse_id");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.fecha = UtilSql.getDateValue(result, "fecha", "dd-MM-yyyy");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.isactive = UtilSql.getValue(result, "isactive");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.docstatus = UtilSql.getValue(result, "docstatus");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.docstatusr = UtilSql.getValue(result, "docstatusr");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.procesar = UtilSql.getValue(result, "procesar");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.procesarBtn = UtilSql.getValue(result, "procesar_btn");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.asvPedidoInventarioId = UtilSql.getValue(result, "asv_pedido_inventario_id");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.language = UtilSql.getValue(result, "language");
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.adUserClient = "";
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.adOrgClient = "";
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.createdby = "";
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.trBgcolor = "";
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.totalCount = "";
        objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PedidoInventarioB7272964605B406992E8D5764C6960F8Data objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[] = new PedidoInventarioB7272964605B406992E8D5764C6960F8Data[vector.size()];
    vector.copyInto(objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data);
    return(objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data);
  }

/**
Create a registry
 */
  public static PedidoInventarioB7272964605B406992E8D5764C6960F8Data[] set(String cBpartnerId, String createdby, String createdbyr, String updatedby, String updatedbyr, String asvPedidoInventarioId, String docstatus, String isactive, String adClientId, String adOrgId, String mWarehouseId, String procesar, String procesarBtn, String fecha)    throws ServletException {
    PedidoInventarioB7272964605B406992E8D5764C6960F8Data objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[] = new PedidoInventarioB7272964605B406992E8D5764C6960F8Data[1];
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0] = new PedidoInventarioB7272964605B406992E8D5764C6960F8Data();
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].created = "";
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].createdbyr = createdbyr;
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].updated = "";
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].updatedTimeStamp = "";
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].updatedby = updatedby;
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].updatedbyr = updatedbyr;
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].adOrgId = adOrgId;
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].adOrgIdr = "";
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].mWarehouseId = mWarehouseId;
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].cBpartnerId = cBpartnerId;
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].fecha = fecha;
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].isactive = isactive;
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].docstatus = docstatus;
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].docstatusr = "";
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].procesar = procesar;
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].procesarBtn = procesarBtn;
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].asvPedidoInventarioId = asvPedidoInventarioId;
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].adClientId = adClientId;
    objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data[0].language = "";
    return objectPedidoInventarioB7272964605B406992E8D5764C6960F8Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef0B59B07BE6C04AAD9083A59BB3DBA16F_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef31F0E57CE9214C7C9A52947ED357827F_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE asv_pedido_inventario" +
      "        SET AD_Org_ID = (?) , M_Warehouse_ID = (?) , C_Bpartner_ID = (?) , Fecha = TO_DATE(?) , Isactive = (?) , Docstatus = (?) , procesar = (?) , ASV_Pedido_Inventario_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE asv_pedido_inventario.ASV_Pedido_Inventario_ID = ? " +
      "        AND asv_pedido_inventario.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND asv_pedido_inventario.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, asvPedidoInventarioId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, asvPedidoInventarioId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO asv_pedido_inventario " +
      "        (AD_Org_ID, M_Warehouse_ID, C_Bpartner_ID, Fecha, Isactive, Docstatus, procesar, ASV_Pedido_Inventario_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, asvPedidoInventarioId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM asv_pedido_inventario" +
      "        WHERE asv_pedido_inventario.ASV_Pedido_Inventario_ID = ? " +
      "        AND asv_pedido_inventario.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND asv_pedido_inventario.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM asv_pedido_inventario" +
      "         WHERE asv_pedido_inventario.ASV_Pedido_Inventario_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM asv_pedido_inventario" +
      "         WHERE asv_pedido_inventario.ASV_Pedido_Inventario_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
