//Sqlc generated V1.O00-1
package com.atrums.nomina.ad_process;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class NOCBPartnerData implements FieldProvider {
static Logger log4j = Logger.getLogger(NOCBPartnerData.class);
  private String InitRecordNumber="0";
  public String mes;
  public String anio;
  public String rolpago;
  public String tercero;
  public String email;
  public String organizacion;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("mes"))
      return mes;
    else if (fieldName.equalsIgnoreCase("anio"))
      return anio;
    else if (fieldName.equalsIgnoreCase("rolpago"))
      return rolpago;
    else if (fieldName.equalsIgnoreCase("tercero"))
      return tercero;
    else if (fieldName.equalsIgnoreCase("email"))
      return email;
    else if (fieldName.equalsIgnoreCase("organizacion"))
      return organizacion;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static NOCBPartnerData[] select(ConnectionProvider connectionProvider, String rolpagoId)    throws ServletException {
    return select(connectionProvider, rolpagoId, 0, 0);
  }

  public static NOCBPartnerData[] select(ConnectionProvider connectionProvider, String rolpagoId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		select  no_numeros_meses (cast (pe.periodno as int))AS MES, " +
      "		CAST( extract(YEAR From NOW())AS INT) AS ANIO, " +
      "		rp.no_rol_pago_provision_id as rolpago, " +
      "		rp.c_bpartner_id as tercero,  us.email as email," +
      "		rp.ad_org_id as organizacion  " +
      "		from no_rol_pago_provision rp, ad_user us, c_period pe, c_bpartner em				" +
      "		where us.c_bpartner_id = rp.c_bpartner_id" +
      "		and pe.c_period_id = rp.c_period_id" +
      "		and rp.c_bpartner_id = em.c_bpartner_id" +
      "		and us.em_no_rol_mail = 'Y'" +
      "		and rp.ispago='Y'" +
      "		and rp.isactive = 'Y' " +
      "		and rp.no_rol_pago_provision_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rolpagoId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NOCBPartnerData objectNOCBPartnerData = new NOCBPartnerData();
        objectNOCBPartnerData.mes = UtilSql.getValue(result, "mes");
        objectNOCBPartnerData.anio = UtilSql.getValue(result, "anio");
        objectNOCBPartnerData.rolpago = UtilSql.getValue(result, "rolpago");
        objectNOCBPartnerData.tercero = UtilSql.getValue(result, "tercero");
        objectNOCBPartnerData.email = UtilSql.getValue(result, "email");
        objectNOCBPartnerData.organizacion = UtilSql.getValue(result, "organizacion");
        objectNOCBPartnerData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNOCBPartnerData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NOCBPartnerData objectNOCBPartnerData[] = new NOCBPartnerData[vector.size()];
    vector.copyInto(objectNOCBPartnerData);
    return(objectNOCBPartnerData);
  }

  public static NOCBPartnerData[] selectEmployee(ConnectionProvider connectionProvider)    throws ServletException {
    return selectEmployee(connectionProvider, 0, 0);
  }

  public static NOCBPartnerData[] selectEmployee(ConnectionProvider connectionProvider, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      	SELECT part.c_bpartner_id as tercero FROM C_BPARTNER part" +
      "		LEFT OUTER JOIN AD_USER  ON part.c_bpartner_id = AD_USER.c_bpartner_id" +
      "		WHERE  AD_USER.c_bpartner_id IS NULL";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NOCBPartnerData objectNOCBPartnerData = new NOCBPartnerData();
        objectNOCBPartnerData.tercero = UtilSql.getValue(result, "tercero");
        objectNOCBPartnerData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNOCBPartnerData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NOCBPartnerData objectNOCBPartnerData[] = new NOCBPartnerData[vector.size()];
    vector.copyInto(objectNOCBPartnerData);
    return(objectNOCBPartnerData);
  }
}
