//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.importaciones.ProcesarImportaciones;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class ProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data implements FieldProvider {
static Logger log4j = Logger.getLogger(ProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String procesado;
  public String value;
  public String name;
  public String ccoCuentaCostosId;
  public String ccoCuentaCostosIdr;
  public String total;
  public String procesar;
  public String isactive;
  public String impTotalProcesarId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("procesado"))
      return procesado;
    else if (fieldName.equalsIgnoreCase("value"))
      return value;
    else if (fieldName.equalsIgnoreCase("name"))
      return name;
    else if (fieldName.equalsIgnoreCase("cco_cuenta_costos_id") || fieldName.equals("ccoCuentaCostosId"))
      return ccoCuentaCostosId;
    else if (fieldName.equalsIgnoreCase("cco_cuenta_costos_idr") || fieldName.equals("ccoCuentaCostosIdr"))
      return ccoCuentaCostosIdr;
    else if (fieldName.equalsIgnoreCase("total"))
      return total;
    else if (fieldName.equalsIgnoreCase("procesar"))
      return procesar;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("imp_total_procesar_id") || fieldName.equals("impTotalProcesarId"))
      return impTotalProcesarId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static ProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static ProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(imp_total_procesar.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = imp_total_procesar.CreatedBy) as CreatedByR, " +
      "        to_char(imp_total_procesar.Updated, ?) as updated, " +
      "        to_char(imp_total_procesar.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        imp_total_procesar.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = imp_total_procesar.UpdatedBy) as UpdatedByR," +
      "        imp_total_procesar.AD_Org_ID, " +
      "(CASE WHEN imp_total_procesar.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "COALESCE(imp_total_procesar.Procesado, 'N') AS Procesado, " +
      "imp_total_procesar.Value, " +
      "imp_total_procesar.Name, " +
      "imp_total_procesar.CCO_Cuenta_Costos_ID, " +
      "(CASE WHEN imp_total_procesar.CCO_Cuenta_Costos_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS CCO_Cuenta_Costos_IDR, " +
      "imp_total_procesar.Total, " +
      "imp_total_procesar.Procesar, " +
      "COALESCE(imp_total_procesar.Isactive, 'N') AS Isactive, " +
      "imp_total_procesar.IMP_Total_Procesar_ID, " +
      "imp_total_procesar.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM imp_total_procesar left join (select AD_Org_ID, Name from AD_Org) table1 on (imp_total_procesar.AD_Org_ID = table1.AD_Org_ID) left join (select CCO_Cuenta_Costos_ID, Name from CCO_Cuenta_Costos) table2 on (imp_total_procesar.CCO_Cuenta_Costos_ID = table2.CCO_Cuenta_Costos_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND imp_total_procesar.IMP_Total_Procesar_ID = ? " +
      "        AND imp_total_procesar.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND imp_total_procesar.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data = new ProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data();
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.created = UtilSql.getValue(result, "created");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.updated = UtilSql.getValue(result, "updated");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.procesado = UtilSql.getValue(result, "procesado");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.value = UtilSql.getValue(result, "value");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.name = UtilSql.getValue(result, "name");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.ccoCuentaCostosId = UtilSql.getValue(result, "cco_cuenta_costos_id");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.ccoCuentaCostosIdr = UtilSql.getValue(result, "cco_cuenta_costos_idr");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.total = UtilSql.getValue(result, "total");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.procesar = UtilSql.getValue(result, "procesar");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.isactive = UtilSql.getValue(result, "isactive");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.impTotalProcesarId = UtilSql.getValue(result, "imp_total_procesar_id");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.language = UtilSql.getValue(result, "language");
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.adUserClient = "";
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.adOrgClient = "";
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.createdby = "";
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.trBgcolor = "";
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.totalCount = "";
        objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[] = new ProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[vector.size()];
    vector.copyInto(objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data);
    return(objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data);
  }

/**
Create a registry
 */
  public static ProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[] set(String name, String ccoCuentaCostosId, String value, String procesar, String createdby, String createdbyr, String adClientId, String adOrgId, String procesado, String total, String impTotalProcesarId, String isactive, String updatedby, String updatedbyr)    throws ServletException {
    ProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[] = new ProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[1];
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0] = new ProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data();
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].created = "";
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].createdbyr = createdbyr;
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].updated = "";
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].updatedTimeStamp = "";
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].updatedby = updatedby;
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].updatedbyr = updatedbyr;
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].adOrgId = adOrgId;
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].adOrgIdr = "";
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].procesado = procesado;
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].value = value;
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].name = name;
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].ccoCuentaCostosId = ccoCuentaCostosId;
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].ccoCuentaCostosIdr = "";
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].total = total;
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].procesar = procesar;
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].isactive = isactive;
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].impTotalProcesarId = impTotalProcesarId;
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].adClientId = adClientId;
    objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data[0].language = "";
    return objectProcesarImportaciones5A2518CE725C40DEB15E55F3A8F94526Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef6DFEFE1BD63C43E09AB113BAE21592B1_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefA93C2FB74B354EB38C8B507D2501B10F_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE imp_total_procesar" +
      "        SET AD_Org_ID = (?) , Procesado = (?) , Value = (?) , Name = (?) , CCO_Cuenta_Costos_ID = (?) , Total = TO_NUMBER(?) , Procesar = (?) , Isactive = (?) , IMP_Total_Procesar_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE imp_total_procesar.IMP_Total_Procesar_ID = ? " +
      "        AND imp_total_procesar.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND imp_total_procesar.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, value);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ccoCuentaCostosId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, total);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, impTotalProcesarId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, impTotalProcesarId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO imp_total_procesar " +
      "        (AD_Org_ID, Procesado, Value, Name, CCO_Cuenta_Costos_ID, Total, Procesar, Isactive, IMP_Total_Procesar_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, value);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ccoCuentaCostosId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, total);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, impTotalProcesarId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM imp_total_procesar" +
      "        WHERE imp_total_procesar.IMP_Total_Procesar_ID = ? " +
      "        AND imp_total_procesar.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND imp_total_procesar.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM imp_total_procesar" +
      "         WHERE imp_total_procesar.IMP_Total_Procesar_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM imp_total_procesar" +
      "         WHERE imp_total_procesar.IMP_Total_Procesar_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
