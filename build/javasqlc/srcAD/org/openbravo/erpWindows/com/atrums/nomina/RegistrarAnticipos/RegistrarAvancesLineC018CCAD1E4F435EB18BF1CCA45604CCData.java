//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.RegistrarAnticipos;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class RegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData implements FieldProvider {
static Logger log4j = Logger.getLogger(RegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String noTipoIngresoEgresoId;
  public String noTipoIngresoEgresoIdr;
  public String valor;
  public String cCurrencyId;
  public String cCurrencyIdr;
  public String isactive;
  public String processed;
  public String includeRolPago;
  public String docstatus;
  public String docstatusr;
  public String docactionno;
  public String docactionnoBtn;
  public String processing;
  public String dateacct;
  public String posted;
  public String postedBtn;
  public String documentno;
  public String cDoctypeId;
  public String adClientId;
  public String noRegistraQuincenaId;
  public String noRegistraQuincLineId;
  public String adOrgId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("no_tipo_ingreso_egreso_id") || fieldName.equals("noTipoIngresoEgresoId"))
      return noTipoIngresoEgresoId;
    else if (fieldName.equalsIgnoreCase("no_tipo_ingreso_egreso_idr") || fieldName.equals("noTipoIngresoEgresoIdr"))
      return noTipoIngresoEgresoIdr;
    else if (fieldName.equalsIgnoreCase("valor"))
      return valor;
    else if (fieldName.equalsIgnoreCase("c_currency_id") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("c_currency_idr") || fieldName.equals("cCurrencyIdr"))
      return cCurrencyIdr;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("include_rol_pago") || fieldName.equals("includeRolPago"))
      return includeRolPago;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("docstatusr"))
      return docstatusr;
    else if (fieldName.equalsIgnoreCase("docactionno"))
      return docactionno;
    else if (fieldName.equalsIgnoreCase("docactionno_btn") || fieldName.equals("docactionnoBtn"))
      return docactionnoBtn;
    else if (fieldName.equalsIgnoreCase("processing"))
      return processing;
    else if (fieldName.equalsIgnoreCase("dateacct"))
      return dateacct;
    else if (fieldName.equalsIgnoreCase("posted"))
      return posted;
    else if (fieldName.equalsIgnoreCase("posted_btn") || fieldName.equals("postedBtn"))
      return postedBtn;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("no_registra_quincena_id") || fieldName.equals("noRegistraQuincenaId"))
      return noRegistraQuincenaId;
    else if (fieldName.equalsIgnoreCase("no_registra_quinc_line_id") || fieldName.equals("noRegistraQuincLineId"))
      return noRegistraQuincLineId;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static RegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String noRegistraQuincenaId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, noRegistraQuincenaId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static RegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String noRegistraQuincenaId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(no_registra_quinc_line.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_registra_quinc_line.CreatedBy) as CreatedByR, " +
      "        to_char(no_registra_quinc_line.Updated, ?) as updated, " +
      "        to_char(no_registra_quinc_line.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        no_registra_quinc_line.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_registra_quinc_line.UpdatedBy) as UpdatedByR," +
      "        no_registra_quinc_line.C_Bpartner_ID, " +
      "(CASE WHEN no_registra_quinc_line.C_Bpartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS C_Bpartner_IDR, " +
      "no_registra_quinc_line.NO_Tipo_Ingreso_Egreso_ID, " +
      "(CASE WHEN no_registra_quinc_line.NO_Tipo_Ingreso_Egreso_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS NO_Tipo_Ingreso_Egreso_IDR, " +
      "no_registra_quinc_line.Valor, " +
      "no_registra_quinc_line.C_Currency_ID, " +
      "(CASE WHEN no_registra_quinc_line.C_Currency_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.ISO_Code), ''))),'') ) END) AS C_Currency_IDR, " +
      "COALESCE(no_registra_quinc_line.Isactive, 'N') AS Isactive, " +
      "COALESCE(no_registra_quinc_line.Processed, 'N') AS Processed, " +
      "COALESCE(no_registra_quinc_line.Include_Rol_Pago, 'N') AS Include_Rol_Pago, " +
      "no_registra_quinc_line.docstatus, " +
      "(CASE WHEN no_registra_quinc_line.docstatus IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS docstatusR, " +
      "no_registra_quinc_line.Docactionno, " +
      "list2.name as Docactionno_BTN, " +
      "no_registra_quinc_line.Processing, " +
      "no_registra_quinc_line.Dateacct, " +
      "no_registra_quinc_line.Posted, " +
      "list3.name as Posted_BTN, " +
      "no_registra_quinc_line.Documentno, " +
      "no_registra_quinc_line.C_Doctype_ID, " +
      "no_registra_quinc_line.AD_Client_ID, " +
      "no_registra_quinc_line.NO_Registra_Quincena_ID, " +
      "no_registra_quinc_line.NO_Registra_Quinc_Line_ID, " +
      "no_registra_quinc_line.AD_Org_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM no_registra_quinc_line left join (select C_BPartner_ID, Name from C_BPartner) table1 on (no_registra_quinc_line.C_Bpartner_ID = table1.C_BPartner_ID) left join (select NO_Tipo_Ingreso_Egreso_ID, Name from NO_Tipo_Ingreso_Egreso) table2 on (no_registra_quinc_line.NO_Tipo_Ingreso_Egreso_ID = table2.NO_Tipo_Ingreso_Egreso_ID) left join (select C_Currency_ID, ISO_Code from C_Currency) table3 on (no_registra_quinc_line.C_Currency_ID = table3.C_Currency_ID) left join ad_ref_list_v list1 on (no_registra_quinc_line.docstatus = list1.value and list1.ad_reference_id = '7A4D156C7DD94868A3D05363B9EF1AB1' and list1.ad_language = ?)  left join ad_ref_list_v list2 on (list2.ad_reference_id = '7A4D156C7DD94868A3D05363B9EF1AB1' and list2.ad_language = ?  AND no_registra_quinc_line.Docactionno = TO_CHAR(list2.value)) left join ad_ref_list_v list3 on (list3.ad_reference_id = '234' and list3.ad_language = ?  AND no_registra_quinc_line.Posted = TO_CHAR(list3.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((noRegistraQuincenaId==null || noRegistraQuincenaId.equals(""))?"":"  AND no_registra_quinc_line.NO_Registra_Quincena_ID = ?  ");
    strSql = strSql + 
      "        AND no_registra_quinc_line.NO_Registra_Quinc_Line_ID = ? " +
      "        AND no_registra_quinc_line.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND no_registra_quinc_line.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (noRegistraQuincenaId != null && !(noRegistraQuincenaId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRegistraQuincenaId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        RegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData = new RegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData();
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.created = UtilSql.getValue(result, "created");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.updated = UtilSql.getValue(result, "updated");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.updatedby = UtilSql.getValue(result, "updatedby");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.noTipoIngresoEgresoId = UtilSql.getValue(result, "no_tipo_ingreso_egreso_id");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.noTipoIngresoEgresoIdr = UtilSql.getValue(result, "no_tipo_ingreso_egreso_idr");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.valor = UtilSql.getValue(result, "valor");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.cCurrencyId = UtilSql.getValue(result, "c_currency_id");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.cCurrencyIdr = UtilSql.getValue(result, "c_currency_idr");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.isactive = UtilSql.getValue(result, "isactive");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.processed = UtilSql.getValue(result, "processed");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.includeRolPago = UtilSql.getValue(result, "include_rol_pago");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.docstatus = UtilSql.getValue(result, "docstatus");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.docstatusr = UtilSql.getValue(result, "docstatusr");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.docactionno = UtilSql.getValue(result, "docactionno");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.docactionnoBtn = UtilSql.getValue(result, "docactionno_btn");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.processing = UtilSql.getValue(result, "processing");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.dateacct = UtilSql.getDateValue(result, "dateacct", "dd-MM-yyyy");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.posted = UtilSql.getValue(result, "posted");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.postedBtn = UtilSql.getValue(result, "posted_btn");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.documentno = UtilSql.getValue(result, "documentno");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.noRegistraQuincenaId = UtilSql.getValue(result, "no_registra_quincena_id");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.noRegistraQuincLineId = UtilSql.getValue(result, "no_registra_quinc_line_id");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.language = UtilSql.getValue(result, "language");
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.adUserClient = "";
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.adOrgClient = "";
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.createdby = "";
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.trBgcolor = "";
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.totalCount = "";
        objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    RegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[] = new RegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[vector.size()];
    vector.copyInto(objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData);
    return(objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData);
  }

/**
Create a registry
 */
  public static RegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[] set(String noRegistraQuincenaId, String includeRolPago, String isactive, String documentno, String processed, String adClientId, String noTipoIngresoEgresoId, String createdby, String createdbyr, String posted, String postedBtn, String noRegistraQuincLineId, String cDoctypeId, String valor, String docactionno, String docactionnoBtn, String adOrgId, String dateacct, String updatedby, String updatedbyr, String docstatus, String processing, String cCurrencyId, String cBpartnerId, String cBpartnerIdr)    throws ServletException {
    RegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[] = new RegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[1];
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0] = new RegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData();
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].created = "";
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].createdbyr = createdbyr;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].updated = "";
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].updatedTimeStamp = "";
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].updatedby = updatedby;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].updatedbyr = updatedbyr;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].cBpartnerId = cBpartnerId;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].cBpartnerIdr = cBpartnerIdr;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].noTipoIngresoEgresoId = noTipoIngresoEgresoId;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].noTipoIngresoEgresoIdr = "";
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].valor = valor;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].cCurrencyId = cCurrencyId;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].cCurrencyIdr = "";
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].isactive = isactive;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].processed = processed;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].includeRolPago = includeRolPago;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].docstatus = docstatus;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].docstatusr = "";
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].docactionno = docactionno;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].docactionnoBtn = docactionnoBtn;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].processing = processing;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].dateacct = dateacct;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].posted = posted;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].postedBtn = postedBtn;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].documentno = documentno;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].cDoctypeId = cDoctypeId;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].adClientId = adClientId;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].noRegistraQuincenaId = noRegistraQuincenaId;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].noRegistraQuincLineId = noRegistraQuincLineId;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].adOrgId = adOrgId;
    objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData[0].language = "";
    return objectRegistrarAvancesLineC018CCAD1E4F435EB18BF1CCA45604CCData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef66B7837835CC466E8C1DFE795D051E51_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefDA50587A772142C7A060954210293929_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefFB257368908847B5B8A79954A1E41810_2(ConnectionProvider connectionProvider, String C_Bpartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as C_Bpartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Bpartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT no_registra_quinc_line.NO_Registra_Quincena_ID AS NAME" +
      "        FROM no_registra_quinc_line" +
      "        WHERE no_registra_quinc_line.NO_Registra_Quinc_Line_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String noRegistraQuincenaId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Nombre), '')) || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))) AS NAME FROM no_registra_quincena left join (select NO_Registra_Quincena_ID, Nombre, C_Period_ID from NO_Registra_Quincena) table1 on (no_registra_quincena.NO_Registra_Quincena_ID = table1.NO_Registra_Quincena_ID) left join (select C_Period_ID, Name from C_Period) table2 on (table1.C_Period_ID =  table2.C_Period_ID) WHERE no_registra_quincena.NO_Registra_Quincena_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRegistraQuincenaId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String noRegistraQuincenaId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Nombre), '')) || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))) AS NAME FROM no_registra_quincena left join (select NO_Registra_Quincena_ID, Nombre, C_Period_ID from NO_Registra_Quincena) table1 on (no_registra_quincena.NO_Registra_Quincena_ID = table1.NO_Registra_Quincena_ID) left join (select C_Period_ID, Name from C_Period) table2 on (table1.C_Period_ID =  table2.C_Period_ID) WHERE no_registra_quincena.NO_Registra_Quincena_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRegistraQuincenaId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE no_registra_quinc_line" +
      "        SET C_Bpartner_ID = (?) , NO_Tipo_Ingreso_Egreso_ID = (?) , Valor = TO_NUMBER(?) , C_Currency_ID = (?) , Isactive = (?) , Processed = (?) , Include_Rol_Pago = (?) , docstatus = (?) , Docactionno = (?) , Processing = (?) , Dateacct = TO_DATE(?) , Posted = (?) , Documentno = (?) , C_Doctype_ID = (?) , AD_Client_ID = (?) , NO_Registra_Quincena_ID = (?) , NO_Registra_Quinc_Line_ID = (?) , AD_Org_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE no_registra_quinc_line.NO_Registra_Quinc_Line_ID = ? " +
      "                 AND no_registra_quinc_line.NO_Registra_Quincena_ID = ? " +
      "        AND no_registra_quinc_line.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_registra_quinc_line.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noTipoIngresoEgresoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, includeRolPago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docactionno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRegistraQuincenaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRegistraQuincLineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRegistraQuincLineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRegistraQuincenaId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO no_registra_quinc_line " +
      "        (C_Bpartner_ID, NO_Tipo_Ingreso_Egreso_ID, Valor, C_Currency_ID, Isactive, Processed, Include_Rol_Pago, docstatus, Docactionno, Processing, Dateacct, Posted, Documentno, C_Doctype_ID, AD_Client_ID, NO_Registra_Quincena_ID, NO_Registra_Quinc_Line_ID, AD_Org_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noTipoIngresoEgresoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, includeRolPago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docactionno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRegistraQuincenaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRegistraQuincLineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String noRegistraQuincenaId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM no_registra_quinc_line" +
      "        WHERE no_registra_quinc_line.NO_Registra_Quinc_Line_ID = ? " +
      "                 AND no_registra_quinc_line.NO_Registra_Quincena_ID = ? " +
      "        AND no_registra_quinc_line.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_registra_quinc_line.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRegistraQuincenaId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM no_registra_quinc_line" +
      "         WHERE no_registra_quinc_line.NO_Registra_Quinc_Line_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM no_registra_quinc_line" +
      "         WHERE no_registra_quinc_line.NO_Registra_Quinc_Line_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
