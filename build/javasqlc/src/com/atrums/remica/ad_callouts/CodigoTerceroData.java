//Sqlc generated V1.O00-1
package com.atrums.remica.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class CodigoTerceroData implements FieldProvider {
static Logger log4j = Logger.getLogger(CodigoTerceroData.class);
  private String InitRecordNumber="0";
  public String codigo;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("codigo"))
      return codigo;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static CodigoTerceroData[] select(ConnectionProvider connectionProvider, String cBpGroupId)    throws ServletException {
    return select(connectionProvider, cBpGroupId, 0, 0);
  }

  public static CodigoTerceroData[] select(ConnectionProvider connectionProvider, String cBpGroupId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select coalesce(max(to_number(value)),0)+1  AS codigo " +
      "      from c_bpartner " +
      "     where c_bp_group_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpGroupId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CodigoTerceroData objectCodigoTerceroData = new CodigoTerceroData();
        objectCodigoTerceroData.codigo = UtilSql.getValue(result, "codigo");
        objectCodigoTerceroData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCodigoTerceroData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CodigoTerceroData objectCodigoTerceroData[] = new CodigoTerceroData[vector.size()];
    vector.copyInto(objectCodigoTerceroData);
    return(objectCodigoTerceroData);
  }

  public static String getLetraGrupo(ConnectionProvider connectionProvider, String cBpGroupId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select substr(max(value),1,1) " +
      "    from c_bp_group " +
      "    where c_bp_group_id = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpGroupId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "substr");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
