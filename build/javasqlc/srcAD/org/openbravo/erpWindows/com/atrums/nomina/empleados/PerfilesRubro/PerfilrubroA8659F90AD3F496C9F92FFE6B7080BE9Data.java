//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.empleados.PerfilesRubro;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class PerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data implements FieldProvider {
static Logger log4j = Logger.getLogger(PerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String nombre;
  public String isactive;
  public String processed;
  public String copiarubro;
  public String nePerfilRubroId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("nombre"))
      return nombre;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("copiarubro"))
      return copiarubro;
    else if (fieldName.equalsIgnoreCase("ne_perfil_rubro_id") || fieldName.equals("nePerfilRubroId"))
      return nePerfilRubroId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static PerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static PerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(ne_perfil_rubro.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ne_perfil_rubro.CreatedBy) as CreatedByR, " +
      "        to_char(ne_perfil_rubro.Updated, ?) as updated, " +
      "        to_char(ne_perfil_rubro.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        ne_perfil_rubro.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = ne_perfil_rubro.UpdatedBy) as UpdatedByR," +
      "        ne_perfil_rubro.AD_Org_ID, " +
      "ne_perfil_rubro.Nombre, " +
      "COALESCE(ne_perfil_rubro.Isactive, 'N') AS Isactive, " +
      "ne_perfil_rubro.Processed, " +
      "ne_perfil_rubro.Copiarubro, " +
      "ne_perfil_rubro.NE_Perfil_Rubro_ID, " +
      "ne_perfil_rubro.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM ne_perfil_rubro" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND ne_perfil_rubro.NE_Perfil_Rubro_ID = ? " +
      "        AND ne_perfil_rubro.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND ne_perfil_rubro.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data = new PerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data();
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.created = UtilSql.getValue(result, "created");
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.updated = UtilSql.getValue(result, "updated");
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.nombre = UtilSql.getValue(result, "nombre");
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.isactive = UtilSql.getValue(result, "isactive");
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.processed = UtilSql.getValue(result, "processed");
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.copiarubro = UtilSql.getValue(result, "copiarubro");
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.nePerfilRubroId = UtilSql.getValue(result, "ne_perfil_rubro_id");
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.language = UtilSql.getValue(result, "language");
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.adUserClient = "";
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.adOrgClient = "";
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.createdby = "";
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.trBgcolor = "";
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.totalCount = "";
        objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[] = new PerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[vector.size()];
    vector.copyInto(objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data);
    return(objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data);
  }

/**
Create a registry
 */
  public static PerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[] set(String processed, String isactive, String createdby, String createdbyr, String nombre, String adOrgId, String copiarubro, String updatedby, String updatedbyr, String adClientId, String nePerfilRubroId)    throws ServletException {
    PerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[] = new PerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[1];
    objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[0] = new PerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data();
    objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[0].created = "";
    objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[0].createdbyr = createdbyr;
    objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[0].updated = "";
    objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[0].updatedTimeStamp = "";
    objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[0].updatedby = updatedby;
    objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[0].updatedbyr = updatedbyr;
    objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[0].adOrgId = adOrgId;
    objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[0].nombre = nombre;
    objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[0].isactive = isactive;
    objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[0].processed = processed;
    objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[0].copiarubro = copiarubro;
    objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[0].nePerfilRubroId = nePerfilRubroId;
    objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[0].adClientId = adClientId;
    objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data[0].language = "";
    return objectPerfilrubroA8659F90AD3F496C9F92FFE6B7080BE9Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef47F1F8EE5C4D49109F76046DFE05CA8E_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefE3C510399DF44EDC9FC7D05C78B813A2_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE ne_perfil_rubro" +
      "        SET AD_Org_ID = (?) , Nombre = (?) , Isactive = (?) , Processed = (?) , Copiarubro = (?) , NE_Perfil_Rubro_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE ne_perfil_rubro.NE_Perfil_Rubro_ID = ? " +
      "        AND ne_perfil_rubro.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ne_perfil_rubro.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nombre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copiarubro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nePerfilRubroId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nePerfilRubroId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO ne_perfil_rubro " +
      "        (AD_Org_ID, Nombre, Isactive, Processed, Copiarubro, NE_Perfil_Rubro_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nombre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copiarubro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nePerfilRubroId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM ne_perfil_rubro" +
      "        WHERE ne_perfil_rubro.NE_Perfil_Rubro_ID = ? " +
      "        AND ne_perfil_rubro.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND ne_perfil_rubro.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM ne_perfil_rubro" +
      "         WHERE ne_perfil_rubro.NE_Perfil_Rubro_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM ne_perfil_rubro" +
      "         WHERE ne_perfil_rubro.NE_Perfil_Rubro_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
