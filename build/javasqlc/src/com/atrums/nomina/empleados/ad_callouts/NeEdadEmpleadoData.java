//Sqlc generated V1.O00-1
package com.atrums.nomina.empleados.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class NeEdadEmpleadoData implements FieldProvider {
static Logger log4j = Logger.getLogger(NeEdadEmpleadoData.class);
  private String InitRecordNumber="0";
  public String edad;
  public String iste;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("edad"))
      return edad;
    else if (fieldName.equalsIgnoreCase("iste"))
      return iste;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static NeEdadEmpleadoData[] select(ConnectionProvider connectionProvider, String emNoFechanacimiento)    throws ServletException {
    return select(connectionProvider, emNoFechanacimiento, 0, 0);
  }

  public static NeEdadEmpleadoData[] select(ConnectionProvider connectionProvider, String emNoFechanacimiento, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		select date_part('year', age(date(?))) as edad, 'N' as iste";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNoFechanacimiento);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NeEdadEmpleadoData objectNeEdadEmpleadoData = new NeEdadEmpleadoData();
        objectNeEdadEmpleadoData.edad = UtilSql.getValue(result, "edad");
        objectNeEdadEmpleadoData.iste = UtilSql.getValue(result, "iste");
        objectNeEdadEmpleadoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNeEdadEmpleadoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NeEdadEmpleadoData objectNeEdadEmpleadoData[] = new NeEdadEmpleadoData[vector.size()];
    vector.copyInto(objectNeEdadEmpleadoData);
    return(objectNeEdadEmpleadoData);
  }
}
