//Sqlc generated V1.O00-1
package com.atrums.nomina.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class NOObtenerCuentasData implements FieldProvider {
static Logger log4j = Logger.getLogger(NOObtenerCuentasData.class);
  private String InitRecordNumber="0";
  public String nDebeAcct;
  public String nHaberAcct;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("n_debe_acct") || fieldName.equals("nDebeAcct"))
      return nDebeAcct;
    else if (fieldName.equalsIgnoreCase("n_haber_acct") || fieldName.equals("nHaberAcct"))
      return nHaberAcct;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static NOObtenerCuentasData[] obtenerCuentas(ConnectionProvider connectionProvider, String noTipoIngresoEgresoId)    throws ServletException {
    return obtenerCuentas(connectionProvider, noTipoIngresoEgresoId, 0, 0);
  }

  public static NOObtenerCuentasData[] obtenerCuentas(ConnectionProvider connectionProvider, String noTipoIngresoEgresoId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		SELECT N_DEBE_ACCT, N_HABER_ACCT " +
      "		FROM no_tipo_ing_egr_acct" +
      "		WHERE " +
      "		NO_TIPO_INGRESO_EGRESO_ID = ?		    ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noTipoIngresoEgresoId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NOObtenerCuentasData objectNOObtenerCuentasData = new NOObtenerCuentasData();
        objectNOObtenerCuentasData.nDebeAcct = UtilSql.getValue(result, "n_debe_acct");
        objectNOObtenerCuentasData.nHaberAcct = UtilSql.getValue(result, "n_haber_acct");
        objectNOObtenerCuentasData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNOObtenerCuentasData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NOObtenerCuentasData objectNOObtenerCuentasData[] = new NOObtenerCuentasData[vector.size()];
    vector.copyInto(objectNOObtenerCuentasData);
    return(objectNOObtenerCuentasData);
  }
}
